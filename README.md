# Simple C++ Reflection System

- [Description](#Description)
    - [Design goals](#Design-goals)
- [Instalation](#Instalation)
    - [Manually](#Manually)
    - [Package manager](#Package-manager)
    - [Dependencies](#Dependencies)
- [Integration](#Integration)
    - [Abstract classes](#Abstract-classes)
- [Examples](#Examples)
    - [Creating object from class name](#Creating-object-from-class-name)
    - [Reflection cast](#Reflection-cast)
    - [Editing members from string](#Editing-members-from-string)
    - [Calling methods from string](#Calling-methods-from-string)
        - [Method overloads](#Method-overloads)
    - [Getting information about the object](#Getting-information-about-the-object)
    - [Serialization](#Serialization)
        - [Serialization of custom types](#Serialization-of-custom-types)
    - [Enums](#Enums)
    - [Setters and Getters](#Setters-and-getters)
    - [Extending reflection functionality](#Extending-reflection-functionality)
- [Full API description](#Full-api-description)
    - [Different ways to get Reflection::Class](#Different-ways-to-get-reflection::class)

# Description
TODO

[Reflection](https://en.wikipedia.org/wiki/Reflection_(computer_programming)) or [RTTI](https://en.wikipedia.org/wiki/Run-time_type_information) 

//allows inspection of classes, interfaces, fields and methods at runtime without knowing the names of the interfaces, fields, methods at compile time. It also allows instantiation of new objects and invocation of methods.

## Design goals

I've seen plenty of C++ reflection systems that are much more complicated than they need to be, so I wanted few things when making this one:

- No preprocessor, no additional compile parameters or extra compile steps.
- As few different macros and functions to remember as possible.
- The ones that are there should be as universal as possible.
- No additional data passed as strings nor unnamed macro parameters, all possible data should be obvious or should work with autocompleters, Intellisense etc.

# Instalation

## Manually

## Package manager

## Dependencies

- [nlohmann::json library](https://github.com/nlohmann/json) is used for converting C++ types to and from string and in serialization.

# Integration
[ReflectionApi.h](https://bitbucket.org/b617/c-simple-reflection-system/src/master/CppReflection/Reflection/ReflectionApi.h) is the one necessary include. It contains all macros used exposed by the library and includes remaining files required for the system to work.

For your class to be reflectable, it has to derive (directly or indirectly) from [Reflection::Object](https://bitbucket.org/b617/c-simple-reflection-system/src/master/CppReflection/Reflection/Object.h). Virtual inheritance is adviced in this case to avoid the [Diamond Inheritance Problem](https://en.wikipedia.org/wiki/Multiple_inheritance#The_diamond_problem).

Furthermore in class declaration, you need to add __DECLARE_REFLECTION_CLASS()__ macro and in the implementation file you need to add __IMPLEMENT_REFLECTION_CLASS(this_class_name)__ macro defining class members and functions which will be reflectable.

__MyClass.h__
```cpp
#include <Reflection/ReflectionApi.h>

class MyClass : public virtual Reflection::Object
{
    DECLARE_REFLECTION_CLASS(); // Adds reflection to this class
public:
    bool IsStringEmpty() const { return stringVar.empty(); }
    void SetString(const std::string& newString);
private:
    std::string stringVar;
    int setCount = 0;
};
```
__MyClass.cpp__
```cpp
#include "MyClass.h"

IMPLEMENT_REFLECTION_CLASS(MyClass, Reflection::Object)
{
    ADD_FIELD(stringVar);
    ADD_FIELD(setCount);

    ADD_METHOD(bool, IsStringEmpty, ); // Comma with nothing at the end denotes that this method doesn't take any parameters
    ADD_METHOD(void, SetString, const std::string&);
}

// Implementation of class methods: ...
```

Once a member is declared in a reflection, it's also available to all of classes deriving from that class.
```cpp
// DerivedClass.h
class DerivedClass : public MyClass
{
    DECLARE_REFLECTION_CLASS();
private:
    float fPoint = 0.0f;
};

// DerivedClass.cpp
#include "DerivedClass.h"

IMPLEMENT_REFLECTION_CLASS(DerivedClass, MyClass)
{
    // Members declared in MyClass are already defined
    ADD_FIELD(fPoint);
}
```
## Abstract classes

By default when declaring reflection in a class like that, the object is expected to have a public parameterless constructor, which would be used if method **CreateInstance()** would be called on [Reflection::Class](https://bitbucket.org/b617/c-simple-reflection-system/src/master/CppReflection/Reflection/Class.h) representing this class in reflection system.

Some classes cannot provide public constructor like that and/or have pure virtual members, so we cannot create a direct instance of this class. For those classes the declaration part remains the same, but you have to use **IMPLEMENT_REFLECTION_ABSTRACT_CLASS()** macro instead of standard **IMPLEMENT_REFLECTION_CLASS()**.

```cpp
// AbstractClass.h
class AbstractClass : public virtual Reflection::Object
{
    DECLARE_REFLECTION_CLASS();
public:
    virtual void Execute() = 0;
protected:
    int executeCount = 0;
};

// AbstractClass.cpp
IMPLEMENT_REFLECTION_ABSTRACT_CLASS()
{
    ADD_METHOD(void, Execute, );
    ADD_FIELD(executeCount);
}
```

The macro is used in the exact same way as the **IMPLEMENT_REFLECTION_CLASS()**.

For classes defined like that **CreateInstance()** will return **nullptr** and **IsAbstract()** will return **true**.

# Examples

## Creating object from class name

```cpp
std::string class_name = "MyNamespace::MyCoolClass";

const Reflection::Class* objectClass = Reflection::Class::FindClassByName(class_name);

// newObject is an instance of MyNamespace::MyCoolClass
Reflection::Object* newObject = objectClass ? objectClass->CreateObject() : nullptr;
```

## Reflection cast

You can use **reflection_cast** to downcast object the same way you'd normally use **dynamic_cast**. It should be faster in an average case.
__TODO:__ make measurements!
``` cpp
class DerivedClass : public MyClass
{
    DECLARE_REFLECTION_CLASS();
    /*...*/
};

//////////////////
MyClass* object = new DerivedClass();
DerivedClass* derived = reflection_cast<DerivedClass*>(object); // Works

//////////////////
MyClass* object = new MyClass();
DerivedClass* derived = reflection_cast<DerivedClass*>(object); // nullptr
```

## Editing members from string
```cpp
class MyClass : public virtual Reflection::Object
{
    DECLARE_REFLECTION_CLASS();
    /*...*/
    int myVariable = 0;
};

//////////////////
std::string fieldName = "myVariable";

MyClass* object = new MyClass();
const Reflection::Class* objectClass = object->GetClass();
const Reflection::Field* field = objectClass->FindField(fieldName);

bool success = field->SetValueFromString(object, "3"); // Returns false if string couldn't be parsed into variable's type
// object->myVariable == 3

object->myVariable = -1;
std::string value = field->GetValueAsString(object); // value == "-1"

//////////////////
// Or if you know the field's type:
int* myVariablePtr = field->AsType<int>()->GetPointer(object);
//...and then you just operate on a pointer to the member variable
```
__Note:__ You don't need to make your members public, reflection system can access variables at any access level as long as they're declared in **IMPLEMENT_REFLECTION_CLASS** macro.

## Calling methods from string
``` cpp
class MyClass : public virtual Reflection::Object
{
    DECLARE_REFLECTION_CLASS();
public:
    void PrintHelloWorld();
    int PrintString(const std::string& message);
    int GetNumberMessagesPrinted() const;
    int Add(int num1, int num2) const;
    /*...*/
};

//////////////////
MyClass* object = new MyClass();
const Reflection::Class* objectClass = object->GetClass();
const Reflection::Method* helloWorldMethod = objectClass->FindMethod("PrintHelloWorld");

Reflection::Method::MethodCallResult result;
// First parameter is object we're calling the method on,
// Second parameter is where to put return value if there is any,
// Third parameter is array of strings: method parameters, in this case
// empty, because method PrintHelloWorld doesn't take any parameters
result = helloWorldMethod->CallFromString(object, nullptr, {});

int addResult = 0;
objectClass->FindMethod("Add")->CallFromString(object, &addResult, {"1", "2"});
// addResult == 3

//////////////////
// Or if you know the method type:
const Reflection::Method* printStringMethod = objectClass->FindMethod("PrintString");
// First parameter of Cast<> is return value type
// All parameters after that are method parameter types
// Invoke returns the same thing as returned by the actual method
int functionReturnValue = printStringMethod->Cast<int, const std::string&>()->Invoke(object, "Hello world!");
```

### Method overloads

If you have multiple methods in a class with the same name but different parameter types, you can find a specific one using **FindMethodOverload&lt;returnType, parameterTypes...\>()**.
```cpp
// MyClass.h
class MyClass : public virtual Reflection::Object
{
    DECLARE_REFLECTION_CLASS();

    void GetData() const;
    int GetData(EDataType dataType) const;
    float GetData(int indexX, int indexY) const;
};

// MyClass.cpp
IMPLEMENT_REFLECTION_CLASS(MyClass, Reflection::Object)
{
    ADD_METHOD(void, GetData, );
    ADD_METHOD(int, GetData, EDataType);
    ADD_METHOD(float, GetData, int, int);
}

//////////////////
MyClass object;
const Reflection::Class* objectClass = object.GetClass();

const Reflection::Method* parameterlessMethod = objectClass->FindMethodOverload<void>("GetData");
const Reflection::Method* enumParamMethod = objectClass->FindMethodOverload<int, EDataType>("GetData");
const Reflection::Method* twoIntsParamsMethod = objectClass->FindMethodOverload<float, int, int>("GetData");

// Calling simply objectClass->FindMethod("GetData") doesn't guarantee which one of those 3 will be returned.
```

## Getting information about the object
``` cpp
const Reflection::Class* objectClass = Reflection::Class::FindClassByName("MyClass");

std::string className = objectClass->GetName(); // "MyClass"
bool abstract = objectClass->IsAbstract(); // If it's not abstract, you can use objectClass->CreateInstance();. If it's abstract, it'll return nullptr

//TODO: SizeOf()?
```

## Serialization

The library comes with 2 very powerful functions defined in [StringConversions.h](https://bitbucket.org/b617/c-simple-reflection-system/src/master/CppReflection/Reflection/StringConversions.h), making heavy use of [nlohmann/json](https://github.com/nlohmann/json) library.

``` cpp
template<typename T>
std::string Reflection::ToString(const T& value);

template<typename T>
bool Reflection::FromString(const std::string& inString, T& outValue);
```
It can be used on a variety of types:

- Simple types (int, float, std::string, ...).
- Collections of simple types (std::vector\<int\>, std::map\<std::string, float\>, ...).
- Enums declared in reflection and collections of those.
- Objects deriving from [Reflection::Object](https://bitbucket.org/b617/c-simple-reflection-system/src/master/CppReflection/Reflection/Object.h) (serialized as map fieldName -> value).
- Collections of objects deriving from [Reflection::Object](https://bitbucket.org/b617/c-simple-reflection-system/src/master/CppReflection/Reflection/Object.h).
- Pointers to objects deriving from [Reflection::Object](https://bitbucket.org/b617/c-simple-reflection-system/src/master/CppReflection/Reflection/Object.h) (serialized as *"null"* in case of **nullptr** or as className + map fieldName -> value otherwise) and collections of those.
- Etc.

``` cpp
class MyClass : public virtual Reflection::Object
{
    DECLARE_REFLECTION_CLASS();
    /*...*/
    int var1;
    float var2;
    std::string var3;
};

//////////////////
MyClass object;
object.var1 = 2;
object.var2 = 3.001f;
object.var3 = "hello!";

std::string objectSerialized = Reflection::ToString(object); // Serializaed as:
// {
//   "fields": {
//     "var1": 2,
//     "var2": 3.001,
//     "var3": "hello!"
//   }
// }

MyClass object2;
bool success = Reflection::FromString(objectSerialized, object2);
// object2 now has var1==2, var2==3.001 and var3=="hello!"

//////////////////
class DerivedClass : public MyClass
{
    DECLARE_REFLECTION_CLASS();
    /*...*/
    std::vector<int> var4;
    MyClass* var5 = nullptr;
};

//////////////////
MyClass* object = new DerivedClass();
object->var1 = 1;
object->var2 = 2.0f;
object->var3 = "three";
static_cast<DerivedClass*>(object)->var4 = { 1, 0 , 0 };

std::string objectSerialized = Reflection::ToString(object); // Serialized as:
// {
//   "class": "DerivedClass",
//   "fields": {
//     "var1": 1,
//     "var2": 2.0,
//     "var3": "three",
//     "var4": {
//       1,
//       0,
//       0
//     },
//     "var5": null
//   }
// }

MyClass* object2 = nullptr;
bool success2 = Reflection::FromString(objectSerialized, object2);
DerivedClass* object3 = nullptr;
bool success3 = Reflection::FromString(objectSerialized, object3);
// object, object2 and object3 are identical copies
```

### Serialization of custom types

In some cases (i.e. when working with other libraries) you may need to add serialization for some custom type that doesn't derive from [Reflection::Object](https://bitbucket.org/b617/c-simple-reflection-system/src/master/CppReflection/Reflection/Object.h).

In those cases you need to provide [json serialization methods](https://github.com/nlohmann/json#arbitrary-types-conversions) used by [nlohmann::json library](https://github.com/nlohmann/json) which is used under the hood by this reflection library.

## Enums

For enums to be serialized by the reflection library you need to either use [NLOHMANN_JSON_SERIALIZE_ENUM()](https://github.com/nlohmann/json#specializing-enum-conversion) macro or **IMPLEMENT_REFLECTION_ENUM()** macro provided by this library.

Currently both of them are serialized to a single number when serializing them as a class member, I'm working on a fix to make them serialize as string. Nonetheless in both case you can still use [Reflection::FromString() / Reflection::ToString()](#Serialization) on them to convert enum value to string or get an enum from it's string representation.

```cpp
enum EFlags
{
	None = 0,
	Physics = 1,
	SoundFX = 2,
	BGM = 4,
	HUD = 8,
	Rendering = 16
};
// Parameters:
// 1. Enum Type name
// 2. Fallback value used for when Reflection::FromString() fails
// 3. Is flags (true) or standard enumeration (false)?
// 4+. Enum values
IMPLEMENT_REFLECTION_ENUM(EFlags, None, true, ENUM_VALUE(None), ENUM_VALUE(Physics), ENUM_VALUE(SoundFX),
	ENUM_VALUE(BGM), ENUM_VALUE(HUD), ENUM_VALUE(Rendering));

EFlags noSound = Physics | SoundFX | HUD | Rendering;
std::string serialized = Reflection::ToString(noSound);
// serialized == "Physics|SoundFX|HUD|Rendering";
EFlags enum2;
bool success = Reflection::FromString(serialized, enum2);
// enum2 == noSound
```

## Setters and Getters

You can add custom setters and getters for fields defined in reflection. If you do, whenever this member is accessed via reflection, those methods will be called to get/set the value.

Getters/setters can be either member methods of the object or some static functions (or lambdas) defined outside of it.

```cpp
// MyClass.h
class MyClass : public virtual Reflection::Object
{
    DECLARE_REFLECTION_CLASS();
private:
    int var1;
    int var2;
public:
    int GetVar1() const;
    void SetVar1(const int& value);
};

// MyClass.cpp
int Var2Getter(const MyClass* object);

IMPLEMENT_REFLECTION_CLASS(MyClass, Reflection::Object)
{
    ADD_FIELD(var1)->SetGetter(&MyClass::GetVar1)->SetSetter(&MyClass::SetVar1);

    ADD_FIELD(var2)->SetGetter(&Var2Getter)->SetSetter([](MyClass* obj, const int& value) { obj->SetVar1(value); });
}
```

## Extending reflection functionality

# Full API description

## Different ways to get Reflection::Class
