#include "stdafx.h"
#include "CppUnitTest.h"
#include "../InterpreterEngine/TestScriptLanguage/TestScriptLanguageEngine.h"
#include <Reflection/Object.h>
#include "../InterpreterEngine/TestScriptLanguage/Values/ValueString.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

class EngineTestClass : public Reflection::Object
{
	DECLARE_REFLECTION_CLASS();
public:
	int value = 0;
};
IMPLEMENT_REFLECTION_CLASS(EngineTestClass, Reflection::Object)
{
	ADD_FIELD(value);
}


namespace InterpreterEngine_UnitTests
{		
	TEST_CLASS(UnitTest1)
	{
	public:
		
		TEST_METHOD(TestMethod1)
		{
			EngineTestClass obj;
			int i = 0;
			float f = 2.3f;

			Interpreter::TestScriptLanguage::Engine engine;
			engine.AddObject(&obj, "myObject");
			engine.AddObject(new Interpreter::TestScriptLanguage::ValueString("hello"), "stringVar");

			engine.Execute(R"(
				stringVar = "world";
				myObject.value = 3;
			)");

			Assert::AreEqual(engine.GetVariable("stringVar")->AsString().c_str(), "world");
			Assert::AreEqual(obj.value, 3);
		}

	};
}