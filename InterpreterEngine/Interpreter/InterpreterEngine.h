#pragma once
#include "InterpreterValue.h"
#include <Reflection/Field.h>
#include <Reflection/Method.h>

namespace Interpreter
{
	/// Base class defining common interface for all interpreters
	class IEngine
	{
	public:

	// Objects

		/// Registers C++ object to be used in interpreter (object will be visible in global scope)
		virtual void AddObject(Reflection::Object* object, const char* name) = 0;
		virtual void AddObject(Value* object, const char* name) = 0;
		
		/// Removes object registered under a given name. Returns false if no object under this name was found
		virtual bool RemoveObject(const char* name) = 0;
		/// Removes all occurrences of given object from the interpreter. Returns number of entries removed
		virtual unsigned int RemoveObject(Reflection::Object* object) = 0;

	// Functions

		/// Registers C++ function to be used in interpreter. It will be called on a given contextObject
		virtual void AddFunction(Reflection::Method* function, Reflection::Object* contextObject) = 0;
		virtual void AddFunction(Reflection::Method* function, Reflection::Object* contextObject, const char* name) = 0;

	// Getters

		/// Find symbol in interpreter's global scope by name
		virtual Value* GetVariable(const char* name) = 0;
		virtual Value* GetFunction(const char* name) = 0;
		virtual Value* GetSymbol(const char* name) = 0;

	// Core functionality

		/// Runs given code and returns the result
		virtual Value* Execute(const std::string& code) = 0;
	};

}

