#pragma once
#include <Reflection/ReflectionApi.h>
#include <string>

namespace Interpreter
{

	class Value
	{
	public:
		enum Types
		{
			Void = 0,
			Undefined = 1,
			Null = 2,
			Boolean = 4,
			String = 8,
			Number = 16,
			Symbol = String | Number,
			Object = 32,
		};

		virtual Types GetType() const { return Undefined; }

		virtual bool AsBool() const { return false; }
		virtual double AsDouble() const { return 0.0; }
		virtual int AsInteger() const { return 0; }
		virtual std::string AsString() const { return ""; }
		virtual Reflection::Object* AsObject() const { return nullptr; }

	};

}

