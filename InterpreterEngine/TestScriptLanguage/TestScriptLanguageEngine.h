#pragma once
#include <map>
#include "../Interpreter/InterpreterEngine.h"
#include "EngineInternal/TestScriptLanguageVariable.h"
#include "EngineInternal/TestScriptLanguageScope.h"

namespace Interpreter
{
	namespace TestScriptLanguage
	{
		class Engine : public IEngine
		{
		public:
			
		// Objects

			/// Registers C++ object to be used in interpreter (object will be visible in global scope)
			virtual void AddObject(Reflection::Object* object, const char* name) { globalScope.AddObject(object, name); }
			/// Creates a variable with given name and starting value
			virtual void AddObject(Value* object, const char* name) { globalScope.AddObject(object, name); }

			/// Removes object registered under a given name. Returns false if no object under this name was found
			virtual bool RemoveObject(const char* name) { return globalScope.RemoveObject(name); }
			/// Removes all occurrences of given object from the interpreter. Returns number of entries removed
			virtual unsigned int RemoveObject(Reflection::Object* object) { return globalScope.RemoveObject(object); }

		// Functions

			/// Registers C++ function to be used in interpreter. It will be called on a given contextObject
			virtual void AddFunction(Reflection::Method* function, Reflection::Object* contextObject) { globalScope.AddFunction(function, contextObject); }
			virtual void AddFunction(Reflection::Method* function, Reflection::Object* contextObject, const char* name) { globalScope.AddFunction(function, contextObject, name); }

		// Getters

			/// Find symbol in interpreter's global scope by name
			virtual Value* GetVariable(const char* name) { return globalScope.GetVariable(name); }
			virtual Value* GetFunction(const char* name) { return globalScope.GetFunction(name); }
			virtual Value* GetSymbol(const char* name) { return globalScope.GetSymbol(name); }

		// Core functionality

			/// Runs given code and returns the result
			virtual Value* Execute(const std::string& code);

		protected:

			Scope globalScope;

		};


	}
}
