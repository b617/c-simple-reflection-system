#pragma once
#include "../../Interpreter/InterpreterEngine.h"
#include "TestScriptLanguageVariable.h"

namespace Interpreter
{
	namespace TestScriptLanguage
	{

		class StackElement
		{

		public: // Interpreter API
			virtual Value* GetValue() const = 0;
			virtual bool IsConstant() = 0;
			bool IsVariable() { return !IsConstant(); }

		public: // Fields
			StackElement* next = nullptr;
		};


		class StackLiteral : StackElement
		{
		public:
			StackLiteral(Value* value);
			~StackLiteral();

			virtual Value* GetValue() const override { return value; }
			virtual bool IsConstant() override { return true; }

		public: // Fields
			Value* value;
		};

		//#REFACTOR
		class StackVariable : StackElement
		{
		public:
			StackVariable(Variable* variable);
			
			virtual Value* GetValue() const override;
			virtual bool IsConstant() override { return false; }

		public: //Fields
			Variable* var;
		};
	}
}