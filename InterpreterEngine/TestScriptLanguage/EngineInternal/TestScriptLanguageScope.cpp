#include "stdafx.h"
#include "TestScriptLanguageScope.h"
#include "../Values/ValueObject.h"

namespace Interpreter
{
	namespace TestScriptLanguage
	{

		Scope::~Scope()
		{

		}

		void Scope::AddObject(Reflection::Object* object, const char* name)
		{
			Variable var(new ValueObject(object));
			variables.insert_or_assign(name, var);
		}

		void Scope::AddObject(Value* object, const char* name)
		{
			variables.insert_or_assign(name, Variable(object));
		}

		bool Scope::RemoveObject(const char* name)
		{
			return variables.erase(name) > 0;
		}

		unsigned int Scope::RemoveObject(Reflection::Object* object)
		{
// 			auto toRemove = std::remove(variables.begin(), variables.end(), object);
// 			const unsigned int numRemoved = std::distance(toRemove, variables.end());
// 			auto removed = variables.erase(toRemove);
// 			return numRemoved;
			return 0u;
		}

		void Scope::AddFunction(Reflection::Method* function, Reflection::Object* contextObject)
		{
			functions.insert_or_assign(function->GetName(), FunctionContext{ function, contextObject });
		}

		void Scope::AddFunction(Reflection::Method* function, Reflection::Object* contextObject, const char* name)
		{
			functions.insert_or_assign(name, FunctionContext{ function, contextObject });
		}

		Value* Scope::GetVariable(const char* name)
		{
			auto var = variables.find(name);
			return (var != variables.end()) ? var->second.GetValue() : nullptr;
		}

		Value* Scope::GetFunction(const char* name)
		{
			//auto func = functions.find(name);
			//return (func != functions.end())?func->second.
			return nullptr;
		}

		Value* Scope::GetSymbol(const char* name)
		{
			if (auto variable = GetVariable(name))
				return variable;
			else
				return GetFunction(name);
		}

	}
}
