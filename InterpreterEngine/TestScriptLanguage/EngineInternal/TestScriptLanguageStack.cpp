#include "stdafx.h"
#include "TestScriptLanguageStack.h"


namespace Interpreter
{
	namespace TestScriptLanguage
	{

		StackLiteral::StackLiteral(Value* value)
			: value(value)
		{
		}

		StackLiteral::~StackLiteral()
		{
			delete value;
		}

		StackVariable::StackVariable(Variable* variable)
			: var(variable)
		{
		}

		Value* StackVariable::GetValue() const
		{
			return var->GetValue();
		}

	}

}

