#pragma once
#include "TestScriptLanguageVariable.h"
#include <map>
#include <Reflection/ReflectionApi.h>

namespace Interpreter
{
	namespace TestScriptLanguage
	{

		class Scope
		{
		public:
			Scope() = default;
			~Scope();

			/// Registers C++ object to be used in interpreter (object will be visible in global scope)
			void AddObject(Reflection::Object* object, const char* name);
			/// Creates a variable with given name and starting value
			void AddObject(Value* object, const char* name);

			/// Removes object registered under a given name. Returns false if no object under this name was found
			bool RemoveObject(const char* name);
			/// Removes all occurrences of given object from the interpreter. Returns number of entries removed
			unsigned int RemoveObject(Reflection::Object* object);

			// Functions

			/// Registers C++ function to be used in interpreter. It will be called on a given contextObject
			void AddFunction(Reflection::Method* function, Reflection::Object* contextObject);
			void AddFunction(Reflection::Method* function, Reflection::Object* contextObject, const char* name);

			// Getters

			/// Find symbol in interpreter's global scope by name
			Value* GetVariable(const char* name);
			Value* GetFunction(const char* name);
			Value* GetSymbol(const char* name);

		protected:
			// Data needed to call a function
			struct FunctionContext
			{
				Reflection::Method* function = nullptr;
				Reflection::Object* context = nullptr;
			};

			std::map<std::string, Variable> variables;
			std::map<std::string, FunctionContext> functions;
		};
	}
}


