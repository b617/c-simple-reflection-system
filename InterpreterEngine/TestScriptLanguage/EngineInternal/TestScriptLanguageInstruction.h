#pragma once

namespace Interpreter
{
	namespace TestScriptLanguage
	{
		struct Instruction
		{
			enum TYPE
			{
				NO_OP = 0,
				PUSH,
				POP,
				ASSIGN,
				CALL,
				ADD,
				SUB,
				MUL,
				DIV,

			};

			Instruction() : cmd(NO_OP), intArg(0) {}
			Instruction(TYPE cmd, int arg) :cmd(cmd), intArg(arg) {}
			Instruction(TYPE cmd, const std::string& arg) :cmd(cmd), strArg(arg) {}
			~Instruction() {}

			TYPE cmd;
			union
			{
				std::string strArg;
				int intArg;
			};

			/// Instruction stack
			Instruction* next = nullptr;
		};

		class InstructionStack
		{
		public:
			inline constexpr bool IsEmpty() const { return top == nullptr; }
			void Pop() 
			{
				Instruction* prev = top;
				top = top->next;
				delete prev;
			}
			void Push(Instruction* instr)
			{
				instr->next = top;
				top = instr;
			}
			inline constexpr Instruction* Top() const { return top; }

		private:
			Instruction* top = nullptr;
		};
	}
}
