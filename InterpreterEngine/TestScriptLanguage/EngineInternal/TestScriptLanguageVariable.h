#pragma once
#include "../../Interpreter/InterpreterValue.h"

namespace Interpreter
{
	namespace TestScriptLanguage
	{
		class Variable
		{
		public:
			Variable();
			Variable(Value* initialValue);
			~Variable();

			Value* GetValue() const { return value; };
			void SetValue(Value* val);

			bool operator==(const Variable& second) const;

		protected:
			Value* value;
		};
	}
}
