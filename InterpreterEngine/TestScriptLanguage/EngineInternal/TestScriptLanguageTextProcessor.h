#pragma once
#include <string>
#include <vector>
#include <stack>
#include "TestScriptLanguageInstruction.h"

namespace Interpreter
{
	namespace TestScriptLanguage
	{
		namespace TextProcessor
		{
			using StringStack = std::stack<std::string>;

			StringStack SplitIntoCommands(const std::string& sourceCode);
			StringStack SplitIntoSymbols(const std::string& command);
			void OrderReversePolishNotation(StringStack& symbols);
			InstructionStack ConvertIntoInstructions(StringStack& rpnSymbols);
		}
	}
}