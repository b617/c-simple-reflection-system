#include "stdafx.h"
#include "TestScriptLanguageVariable.h"
#include "../Values/ValueVoid.h"


Interpreter::TestScriptLanguage::Variable::Variable()
	: value(new ValueVoid())
{
}

Interpreter::TestScriptLanguage::Variable::~Variable()
{
	delete value;
}

Interpreter::TestScriptLanguage::Variable::Variable(Value* initialValue)
	: value(initialValue)
{
}

void Interpreter::TestScriptLanguage::Variable::SetValue(Value* val)
{
	delete value;
	value = val;
}

bool Interpreter::TestScriptLanguage::Variable::operator==(const Variable& second) const
{
	return second.GetValue() == GetValue();
}
