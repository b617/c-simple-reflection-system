#pragma once
#include "../../Interpreter/InterpreterValue.h"

namespace Interpreter
{
	namespace TestScriptLanguage
	{
		class ValueVoid : public Value
		{
		public:
			ValueVoid() = default;
			virtual Types GetType() const override { return Value::Void; }
		};
	}
}
