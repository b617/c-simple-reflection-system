#pragma once
#include "../../Interpreter/InterpreterValue.h"

namespace Interpreter
{
	namespace TestScriptLanguage
	{

		class ValueString : public Value
		{
		public:
			ValueString() = default;
			ValueString(const std::string& value);


			virtual Types GetType() const override { return Value::String; }


			virtual bool AsBool() const override;
			virtual double AsDouble() const override;
			virtual int AsInteger() const override;
			virtual std::string AsString() const override { return data; }

		protected:
			std::string data;
		};

	}
}
