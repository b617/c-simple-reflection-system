#include "stdafx.h"
#include "ValueObject.h"
#include "Reflection/Class.h"
#include "Reflection/Field.h"


namespace Interpreter
{
	namespace TestScriptLanguage
	{

		ValueObject::ValueObject(Reflection::Object* object)
			: object(object)
		{
		}

		std::string ValueObject::AsString() const
		{
			if (object == nullptr)
			{
				return "nullptr";
			}
			
			auto clazz = object->GetClass();

			std::string str = clazz->GetName();
			for (auto f : clazz->GetFields())
			{
				str += "\n\t";
				str += f->GetName();
				str += ": ";
				str += f->GetValueAsString(object);
			}

			return str;
		}

	}
}
