#include "stdafx.h"
#include "ValueString.h"

Interpreter::TestScriptLanguage::ValueString::ValueString(const std::string& value)
	: data(value)
{
}


bool Interpreter::TestScriptLanguage::ValueString::AsBool() const
{
	return data == "true";
}

double Interpreter::TestScriptLanguage::ValueString::AsDouble() const
{
	try
	{
		return std::stod(data);
	}
	catch (...)
	{
		return 0.0;
	}
}

int Interpreter::TestScriptLanguage::ValueString::AsInteger() const
{
	try
	{
		return std::stoi(data);
	}
	catch (...)
	{
		return 0;
	}
}
