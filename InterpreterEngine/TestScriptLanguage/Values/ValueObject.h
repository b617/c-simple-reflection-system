#pragma once
#include "../../Interpreter/InterpreterValue.h"

namespace Interpreter
{
	namespace TestScriptLanguage
	{

		class ValueObject : public Value
		{
		public:
			ValueObject() = default;
			ValueObject(Reflection::Object* object);

			virtual Types GetType() const override { return Value::Object; }
			virtual bool AsBool() const override { return object != nullptr; }
			virtual std::string AsString() const override;
			virtual Reflection::Object* AsObject() const override { return object; }

		protected:
			Reflection::Object* object = nullptr;
		};
	}
}
