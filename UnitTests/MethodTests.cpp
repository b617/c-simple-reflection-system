#include "stdafx.h"
#include <assert.h>
#include "CppUnitTest.h"
#include "Reflection/ReflectionApi.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

class TestMethodsBase : public Reflection::Object
{
	DECLARE_REFLECTION_CLASS();

public:
	int value = 0;

	void ChangeValueTo1() { value = 1; }
	void ChangeValueTo() { value = 0; }
	void ChangeValueTo(int newValue) { value = newValue; }
	void ChangeValueTo(int a, int b) { value = a + b; }
	int GetValue() const { return value; }

	virtual void VirtualMethod() { value = 4; }

private:
	void PrivateMethod() { value = -1; }

};
IMPLEMENT_REFLECTION_CLASS(TestMethodsBase, Reflection::Object)
{
	ADD_METHOD(void, ChangeValueTo1, );
	ADD_METHOD(void, ChangeValueTo, );
	ADD_METHOD(void, ChangeValueTo, int);
	ADD_METHOD(void, ChangeValueTo, int, int);
	ADD_METHOD(int, GetValue, );
	ADD_METHOD(void, PrivateMethod, );
	ADD_METHOD(void, VirtualMethod, );
}

class TestMethodsDerived : public TestMethodsBase
{
	DECLARE_REFLECTION_CLASS();

public:

	virtual void VirtualMethod() override { value = 15; }
};
IMPLEMENT_REFLECTION_CLASS(TestMethodsDerived, TestMethodsBase)
{
}

#define CAST_METHOD(method, return_val, ...) (static_cast<Reflection::MethodTImpl<TestMethodsBase, return_val, __VA_ARGS__>*>(method))

namespace UnitTests
{
	TEST_CLASS(MethodTests)
	{
		TEST_METHOD(FindMethod)
		{
			// Find methods
			const Reflection::Class* c = TestMethodsBase::STATIC_CLASS;
			Reflection::Method* m1 = c->FindMethod("ChangeValueTo1");
			Assert::IsNotNull(m1);
			Reflection::Method* m2 = c->FindMethod("ChangeValueTo");
			Assert::IsNotNull(m2);
			Reflection::Method* m3 = c->FindMethod("GetValue");	// Should get the first method defined
			Assert::IsNotNull(m3);

			// Check that method objects are different
			Assert::IsTrue(m1 != m2);
			Assert::IsTrue(m1 != m3);
			Assert::IsTrue(m3 != m2);

			// Check if function pointers are correct
			Assert::IsTrue(CAST_METHOD(m1, void)->GetFunctionPointer() == &TestMethodsBase::ChangeValueTo1);
			Assert::IsTrue(CAST_METHOD(m2, void)->GetFunctionPointer() == (void(TestMethodsBase::*)())&TestMethodsBase::ChangeValueTo);
			Assert::IsTrue(CAST_METHOD(m3, int)->GetConstFunctionPointer() == &TestMethodsBase::GetValue);
		}

		TEST_METHOD(FindMethodOverload)
		{
			// Find overrides
			const Reflection::Class* c = TestMethodsBase::STATIC_CLASS;
			Reflection::Method* voidPtr = c->FindMethodOverload<void>("ChangeValueTo");
			Assert::IsNotNull(voidPtr);
			Reflection::Method* intPtr = c->FindMethodOverload<void, int>("ChangeValueTo");
			Assert::IsNotNull(intPtr);
			Reflection::Method* intIntPtr = c->FindMethodOverload<void, int, int>("ChangeValueTo");
			Assert::IsNotNull(intIntPtr);

			// Check that method objects are different
			Assert::IsTrue(voidPtr != intPtr);
			Assert::IsTrue(voidPtr != intIntPtr);
			Assert::IsTrue(intPtr != intIntPtr);

			// Check if the function pointers are correct
			Assert::IsTrue(CAST_METHOD(voidPtr, void)->GetFunctionPointer() == (void(TestMethodsBase::*)())&TestMethodsBase::ChangeValueTo);
			Assert::IsTrue(CAST_METHOD(intPtr, void, int)->GetFunctionPointer() == (void(TestMethodsBase::*)(int))&TestMethodsBase::ChangeValueTo);
			Assert::IsTrue(CAST_METHOD(intIntPtr, void, int, int)->GetFunctionPointer() == (void(TestMethodsBase::*)(int, int))&TestMethodsBase::ChangeValueTo);
		}

		TEST_METHOD(FindMethodOverride)
		{
			// Base method on base object
			TestMethodsBase obj1;
			obj1.GetClass()->FindMethodOverload<void>("VirtualMethod")->Invoke(&obj1);
			Assert::AreEqual(4, obj1.value);

			// Derived method on derived object
			TestMethodsDerived obj2;
			obj2.GetClass()->FindMethodOverload<void>("VirtualMethod")->Invoke(&obj2);
			Assert::AreEqual(15, obj2.value);

			// Base method on derived object
			obj2.value = 0;
			obj1.GetClass()->FindMethodOverload<void>("VirtualMethod")->Invoke(&obj2);
			Assert::AreEqual(15, obj2.value);
		}

		TEST_METHOD(InvokeMethod)
		{
			TestMethodsBase obj;
			const Reflection::Class* c = obj.GetClass();
			//obj.value == 0;

			c->FindMethod("ChangeValueTo1")->Cast<void>()->Invoke(&obj);
			Assert::AreEqual(obj.value, 1);

			c->FindMethodOverload<void>("ChangeValueTo")->Invoke(&obj);
			Assert::AreEqual(obj.value, 0);
			c->FindMethodOverload<void, int>("ChangeValueTo")->Invoke(&obj, -3);
			Assert::AreEqual(obj.value, -3);
			c->FindMethodOverload<void, int, int>("ChangeValueTo")->Invoke(&obj, 2, 3);
			Assert::AreEqual(obj.value, 5);

			Assert::AreEqual(5, c->FindMethod("GetValue")->Cast<int>()->Invoke(&obj));
			obj.value = -17;
			Assert::AreEqual(-17, c->FindMethodOverload<int>("GetValue")->Invoke(&obj));
		}

		TEST_METHOD(InvalidCast)
		{
			const Reflection::Class* c = TestMethodsBase::STATIC_CLASS;
			Reflection::Method* m1 = c->FindMethod("ChangeValueTo1");
			Assert::IsNotNull(m1);
			Assert::IsNotNull(m1->Cast<void>());
			Assert::IsNull(m1->Cast<int>());
		}

		TEST_METHOD(NonExistingMethod)
		{
			const Reflection::Class* c = TestMethodsBase::STATIC_CLASS;

			// Non-existing method
			Assert::IsNull(c->FindMethod("SetValue"));
			Assert::IsNull(c->FindMethodOverload<void>("SetValue"));
			// Non-existing overload
			Assert::IsNull(c->FindMethodOverload<float>("ChangeValueTo"));
		}

		TEST_METHOD(GetParameters)
		{

		}

		TEST_METHOD(InvokeMethodParamsFromString)
		{
			TestMethodsBase obj;
			Reflection::Method::MethodCallResult result;
			const Reflection::Class* c = obj.GetClass();
			Assert::AreEqual(obj.value, 0);

			// Call with parameters
			auto* method = c->FindMethodOverload<void, int>("ChangeValueTo");
			result = method->CallFromString(&obj, nullptr, { "-86" });
			Assert::IsTrue(result == Reflection::Method::Success);
			Assert::AreEqual(obj.value, -86);

			// Call without parameters
			result = c->FindMethod("ChangeValueTo1")->CallFromString(&obj, nullptr, {});
			Assert::IsTrue(result == Reflection::Method::Success);
			Assert::AreEqual(obj.value, 1);

			// Get return value
			obj.value = 228;
			int returnValue;
			result = c->FindMethod("GetValue")->CallFromString(&obj, &returnValue, {});
			Assert::IsTrue(result == Reflection::Method::Success);
			Assert::AreEqual(returnValue, 228);
		}

		TEST_METHOD(InvokeMethodInvalidParamsFromString)
		{
			TestMethodsBase obj;
			Reflection::Method::MethodCallResult result;
			const Reflection::Class* c = obj.GetClass();
			obj.value = 0;

			// Too many parameters
			result = c->FindMethod("ChangeValueTo1")->CallFromString(&obj, nullptr, { "1", "hello", "world" });
			Assert::IsTrue(result == Reflection::Method::TooManyArguments);
			Assert::IsTrue(obj.value == 0);

			// Too few parameters
			result = c->FindMethodOverload<void, int, int>("ChangeValueTo")->CallFromString(&obj, nullptr, { "10" });
			Assert::IsTrue(result = Reflection::Method::TooFewArguments);
			Assert::IsTrue(obj.value == 0);

			// Unable to parse
			result = c->FindMethodOverload<void, int>("ChangeValueTo")->CallFromString(&obj, nullptr, { "hello world" });
			Assert::IsTrue(result = Reflection::Method::UnableToParse);
			Assert::IsTrue(obj.value == 0);
		}

		TEST_METHOD(AccessPrivateMethod)
		{
			TestMethodsBase obj;
			obj.GetClass()->FindMethodOverload<void>("PrivateMethod")->Invoke(&obj);
			Assert::AreEqual(-1, obj.value);
		}
	};

}


#undef CAST_METHOD