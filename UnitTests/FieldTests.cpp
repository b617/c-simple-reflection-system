#include "stdafx.h"
#include <assert.h>
#include "CppUnitTest.h"
#include "Reflection/ReflectionApi.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

class Reflection_TestClass : public Reflection::Object
{
	DECLARE_REFLECTION_CLASS();

public:
	int int_field;
	float float_field;

private:
	int private_int_field;
public:
	int GetPrivateIntField() const { return private_int_field; }

};
IMPLEMENT_REFLECTION_CLASS(Reflection_TestClass, Reflection::Object)
{
	ADD_FIELD(int_field);
	ADD_FIELD(float_field);
	ADD_FIELD(private_int_field);
}

namespace UnitTests
{	
	TEST_CLASS(FieldTests)
	{
	public:
		
		TEST_METHOD(CorrectFieldAddress)
		{
			Reflection_TestClass instance;

			const Reflection::Field* field = instance.GetClass()->FindField("float_field");
			float* casted_field_ptr = field->AsType<float>()->GetPointer(&instance);
			float* c_ptr = &instance.float_field;
			Assert::AreEqual(c_ptr, casted_field_ptr);

			int* casted_field_ptr2 = instance.GetClass()->FindField("int_field")->AsType<int>()->GetPointer(&instance);
			int* c_ptr2 = &instance.int_field;
			Assert::AreEqual(c_ptr2, casted_field_ptr2);
		}

		TEST_METHOD(SetFromString)
		{
			Reflection_TestClass instance;
			
			const Reflection::Field* field1 = instance.GetClass()->FindField("float_field");
			Assert::IsTrue(field1->SetValueFromString(&instance, "2"));
			Assert::AreEqual(2.f, instance.float_field);
			Assert::IsTrue(field1->SetValueFromString(&instance, "-1.6"));
			Assert::AreEqual(-1.6f, instance.float_field);
			Assert::IsTrue(field1->SetValueFromString(&instance, "0.01"));
			Assert::AreEqual(0.01f, instance.float_field);

			const Reflection::Field* field2 = instance.GetClass()->FindField("int_field");
			Assert::IsTrue(field2->SetValueFromString(&instance, "0"));
			Assert::AreEqual(0, instance.int_field);
			Assert::IsTrue(field2->SetValueFromString(&instance, "-1"));
			Assert::AreEqual(-1, instance.int_field);
			Assert::IsTrue(field2->SetValueFromString(&instance, "18649"));
			Assert::AreEqual(18649, instance.int_field);
		}

		TEST_METHOD(SetFromInvalidString)
		{
			// SetValueFromString()s with invalid parameter should not change the field's value

			Reflection_TestClass instance;
			instance.float_field = 1.5f;
			instance.int_field = 7;

			const Reflection::Field* float_field = instance.GetClass()->FindField("float_field");
			const Reflection::Field* int_field = instance.GetClass()->FindField("int_field");

			Assert::IsFalse(float_field->SetValueFromString(&instance, ""));
			Assert::AreEqual(1.5f, instance.float_field);
			Assert::IsFalse(float_field->SetValueFromString(&instance, "a"));
			Assert::AreEqual(1.5f, instance.float_field);
			Assert::IsFalse(float_field->SetValueFromString(&instance, "hello world!0123456789"));
			Assert::AreEqual(1.5f, instance.float_field);

			Assert::IsFalse(int_field->SetValueFromString(&instance, ""));
			Assert::AreEqual(7, instance.int_field);
			Assert::IsFalse(int_field->SetValueFromString(&instance, " "));
			Assert::AreEqual(7, instance.int_field);
			Assert::IsFalse(int_field->SetValueFromString(&instance, "how much wood would woodchuck chuck if woodchuck would chuck wood?"));
			Assert::AreEqual(7, instance.int_field);
		}

		TEST_METHOD(GetAsString)
		{
			Reflection_TestClass instance;
			
			const Reflection::Field* field1 = instance.GetClass()->FindField("float_field");
			instance.float_field = 2.0f;
			Assert::AreEqual("2.0", field1->GetValueAsString(&instance).c_str());
			instance.float_field = -1;
			Assert::AreEqual("-1.0", field1->GetValueAsString(&instance).c_str());
			instance.float_field = 0.0006f;
			Assert::AreEqual("0.0006000000284984708", field1->GetValueAsString(&instance).c_str());

			const Reflection::Field* field2 = instance.GetClass()->FindField("int_field");
			instance.int_field = 6;
			Assert::AreEqual("6", field2->GetValueAsString(&instance).c_str());
			instance.int_field = 0;
			Assert::AreEqual("0", field2->GetValueAsString(&instance).c_str());
			instance.int_field = -7779;
			Assert::AreEqual("-7779", field2->GetValueAsString(&instance).c_str());
		}

		TEST_METHOD(GetFromPointer)
		{
			Reflection_TestClass instance;

			auto* float_field = instance.GetClass()->FindField("float_field")->AsType<float>();
			instance.float_field = 2.0f;
			Assert::AreEqual(2.0f, float_field->GetValue(&instance));
			instance.float_field = -12345.67f;
			Assert::AreEqual(-12345.67f, float_field->GetValue(&instance));

			auto* int_field = Reflection_TestClass::STATIC_CLASS->FindField("int_field")->AsType<int>();
			instance.int_field = 1;
			Assert::AreEqual(1, int_field->GetValue(&instance));
			instance.int_field = -1500109;
			Assert::AreEqual(-1500109, int_field->GetValue(&instance));

		}

		TEST_METHOD(SetFromPointer)
		{
			Reflection_TestClass instance;

			auto* float_field = instance.GetClass()->FindField("float_field")->AsType<float>();
			float_field->SetValue(&instance, 2.5f);
			Assert::AreEqual(2.5f, instance.float_field);			
			float_field->SetValue(&instance, -43.f);
			Assert::AreEqual(-43.f, instance.float_field);

			auto* int_field = Reflection_TestClass::STATIC_CLASS->FindField("int_field")->AsType<int>();
			int_field->SetValue(&instance, 5);
			Assert::AreEqual(5, instance.int_field);
			int_field->SetValue(&instance, -100000);
			Assert::AreEqual(-100000, instance.int_field);
		}

		TEST_METHOD(AccessPrivateMember)
		{
			Reflection_TestClass instance;
			const Reflection::Field* field = instance.GetClass()->FindField("private_int_field");
			auto field_casted = field->AsType<int>();
			
			field->SetValueFromString(&instance, "12");
			Assert::AreEqual(12, instance.GetPrivateIntField());
			Assert::AreEqual("12", field->GetValueAsString(&instance).c_str());

			field->SetValueFromString(&instance, "-7");
			Assert::AreEqual(-7, instance.GetPrivateIntField());
			Assert::AreEqual("-7", field->GetValueAsString(&instance).c_str());

			field->AsType<int>()->SetValue(&instance, 4);
			Assert::AreEqual(4, instance.GetPrivateIntField());
			Assert::AreEqual("4", field->GetValueAsString(&instance).c_str());

		}

		TEST_METHOD(NonExistingField)
		{
			Reflection_TestClass instance;
			const Reflection::Field* field1 = instance.GetClass()->FindField("this_field_doesnt_exist");
			Assert::IsNull(field1);

			const Reflection::Field* field2 = instance.GetClass()->FindField("int_field float_field");
			Assert::IsNull(field2);
		}

	};
}