#include "stdafx.h"
#include <assert.h>
#include "CppUnitTest.h"
#include "Reflection/ReflectionApi.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

#pragma region TEST_TYPES_DEFINITIONS
enum ENormalEnum
{
	SomeValue,
	SomeOtherValue,
	YetAnotherValue,
	EvenAnotherValue
};

IMPLEMENT_REFLECTION_ENUM(ENormalEnum, SomeValue, false, ENUM_VALUE(SomeValue), ENUM_VALUE(SomeOtherValue), 
	ENUM_VALUE(YetAnotherValue), ENUM_VALUE(EvenAnotherValue));


enum EFlags
{
	None = 0,
	Flag1 = 1,
	Flag2 = 2,
	Flag3 = 4,
	Flag4 = 8,
	Flag5 = 16
};
IMPLEMENT_REFLECTION_ENUM(EFlags, None, true, ENUM_VALUE(None), ENUM_VALUE(Flag1), ENUM_VALUE(Flag2), 
	ENUM_VALUE(Flag3), ENUM_VALUE(Flag4), ENUM_VALUE(Flag5));

namespace TestEnumNS
{
	namespace NestedNS
	{
		enum ENamespaceEnum
		{
			Value0,
			Value1,
			Value2,

			COUNT
		};
	}
}
IMPLEMENT_REFLECTION_ENUM(TestEnumNS::NestedNS::ENamespaceEnum, TestEnumNS::NestedNS::COUNT, false, 
	// Let's check if it works correctly with writing full enum path and just part of it:
	ENUM_VALUE(TestEnumNS::NestedNS::ENamespaceEnum::Value0), ENUM_VALUE(TestEnumNS::NestedNS::Value1), ENUM_VALUE(TestEnumNS::NestedNS::Value2));

enum class EEnumClass
{
	ThisIs,
	AReally,
	NiceEnum,
	AndI,
	WillAlways,
	LikeHim
};
IMPLEMENT_REFLECTION_ENUM(EEnumClass, EEnumClass::NiceEnum, false, // Let's check whether declaration will work out of order:
	ENUM_VALUE(EEnumClass::AndI), ENUM_VALUE(EEnumClass::AReally), ENUM_VALUE(EEnumClass::LikeHim),
	ENUM_VALUE(EEnumClass::NiceEnum), ENUM_VALUE(EEnumClass::ThisIs), ENUM_VALUE(EEnumClass::WillAlways));


class ClassWithEnum : public Reflection::Object
{
	DECLARE_REFLECTION_CLASS();
public:
	enum EEnumInClass
	{
		Val0,
		Val1,
		Val2
	};

	ENormalEnum normalEnum;
	TestEnumNS::NestedNS::ENamespaceEnum namespaceEnum;
	EEnumClass enumClass;
	EEnumInClass enumInClass;
};
IMPLEMENT_REFLECTION_ENUM(ClassWithEnum::EEnumInClass, ClassWithEnum::Val0, false,
	ENUM_VALUE(ClassWithEnum::Val0), ENUM_VALUE(ClassWithEnum::Val1), ENUM_VALUE(ClassWithEnum::Val2));
IMPLEMENT_REFLECTION_CLASS(ClassWithEnum, Reflection::Object)
{
	ADD_FIELD(normalEnum);
	ADD_FIELD(namespaceEnum);
	ADD_FIELD(enumClass);
	ADD_FIELD(enumInClass);
}
#pragma endregion

namespace UnitTests
{
	TEST_CLASS(EnumTests)
	{
		TEST_METHOD(NormalStringConversion)
		{
			ENormalEnum e;
			bool conversion_success;

			e = YetAnotherValue;
			Assert::AreEqual("YetAnotherValue", Reflection::ToString(e).c_str());
			e = EvenAnotherValue;
			Assert::AreEqual("EvenAnotherValue", Reflection::ToString(e).c_str());

			conversion_success = Reflection::FromString("SomeOtherValue", e);
			Assert::IsTrue(conversion_success);
			Assert::AreEqual((int)SomeOtherValue, (int)e);

			conversion_success = Reflection::FromString("NotExistingValue", e);
			Assert::IsFalse(conversion_success);
			Assert::AreEqual((int)SomeValue, (int)e);
		}

		TEST_METHOD(FlagsStringConversion)
		{
			EFlags value = EFlags(Flag1 | Flag3 | Flag4);
			Assert::AreEqual("Flag1|Flag3|Flag4", Reflection::ToString(value).c_str());
			
			value = EFlags::None;
			Assert::AreEqual("None", Reflection::ToString(value).c_str());

			value = EFlags::Flag5;
			Assert::AreEqual("Flag5", Reflection::ToString(value).c_str());


			Assert::IsTrue( Reflection::FromString("Flag2|Flag5", value) );
			Assert::AreEqual(Flag2 | Flag5, (int)value);

			Assert::IsTrue( Reflection::FromString("None", value) );
			Assert::AreEqual((int)EFlags::None, (int)value);

			value = Flag2;
			Assert::IsFalse( Reflection::FromString("", value) );
			Assert::AreEqual((int)EFlags::None, (int)value);
		}

		TEST_METHOD(Namespaces)
		{
			TestEnumNS::NestedNS::ENamespaceEnum e1;
			EEnumClass e2;
			ClassWithEnum::EEnumInClass e3;

			e1 = TestEnumNS::NestedNS::Value2;
			Assert::AreEqual("Value2", Reflection::ToString(e1).c_str());
			
			Assert::IsTrue(Reflection::FromString("Value1", e1));
			Assert::AreEqual(int(TestEnumNS::NestedNS::Value1), int(e1));

			Assert::IsTrue(Reflection::FromString("TestEnumNS::NestedNS::Value0", e1));
			Assert::AreEqual(int(TestEnumNS::NestedNS::Value0), int(e1));
			Assert::AreEqual("Value0", Reflection::ToString(e1).c_str());


			e2 = EEnumClass::NiceEnum;
			Assert::AreEqual("NiceEnum", Reflection::ToString(e2).c_str());
			
			Assert::IsTrue(Reflection::FromString("ThisIs", e2));
			Assert::AreEqual(int(EEnumClass::ThisIs), int(e2));
			
			Assert::IsTrue(Reflection::FromString("EEnumClass::WillAlways", e2));
			Assert::AreEqual(int(EEnumClass::WillAlways), int(e2));


			e3 = ClassWithEnum::Val1;
			Assert::AreEqual("Val1", Reflection::ToString(e3).c_str());

			Assert::IsTrue(Reflection::FromString("Val2", e3));
			Assert::AreEqual(int(ClassWithEnum::Val2), int(e3));

			Assert::IsTrue(Reflection::FromString("ClassWithEnum::Val1", e3));
			Assert::AreEqual(int(ClassWithEnum::Val1), int(e3));
		}

		TEST_METHOD(EnumAsClassField)
		{
			ClassWithEnum c;
			c.normalEnum = EvenAnotherValue;
			c.enumClass = EEnumClass::AReally;
			c.namespaceEnum = TestEnumNS::NestedNS::Value2;
			c.enumInClass = ClassWithEnum::Val0;

			const Reflection::Class* clazz = c.GetClass();
			
			Assert::AreEqual("EvenAnotherValue", clazz->FindField("normalEnum")->GetValueAsString(&c).c_str());
			Assert::AreEqual("AReally", clazz->FindField("enumClass")->GetValueAsString(&c).c_str());
			Assert::AreEqual("Value2", clazz->FindField("namespaceEnum")->GetValueAsString(&c).c_str());
			Assert::AreEqual("Val0", clazz->FindField("enumInClass")->GetValueAsString(&c).c_str());


			clazz->FindField("normalEnum")->SetValueFromString(&c, "ENormalEnum::SomeValue");
			clazz->FindField("enumClass")->SetValueFromString(&c, "LikeHim");
			clazz->FindField("namespaceEnum")->SetValueFromString(&c, "Value1");
			clazz->FindField("enumInClass")->SetValueFromString(&c, "Val2");

			Assert::AreEqual((int)c.normalEnum, (int)SomeValue);
			Assert::AreEqual((int)c.enumClass, (int)EEnumClass::LikeHim);
			Assert::AreEqual((int)c.namespaceEnum, (int)TestEnumNS::NestedNS::ENamespaceEnum::Value1);
			Assert::AreEqual((int)c.enumInClass, (int)ClassWithEnum::EEnumInClass::Val2);

		}
	};
}