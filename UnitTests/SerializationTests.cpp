#include "stdafx.h"
#include <assert.h>
#include "CppUnitTest.h"
#include "Reflection/ReflectionApi.h"
#include "Serialization/BinarySerializer.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

#pragma region Data declarations

enum ENormalEnum
{
	SomeValue,
	SomeOtherValue,
	YetAnotherValue,
	EvenAnotherValue
};
NLOHMANN_JSON_SERIALIZE_ENUM(ENormalEnum, { {ENUM_VALUE(SomeValue)}, {ENUM_VALUE(SomeOtherValue)},
	{ENUM_VALUE(YetAnotherValue)}, {ENUM_VALUE(EvenAnotherValue)} });

enum EFlags
{
	None = 0,
	Flag1 = 1,
	Flag2 = 2,
	Flag3 = 4,
	Flag4 = 8,
	Flag5 = 16
};
IMPLEMENT_REFLECTION_ENUM(EFlags, None, true, ENUM_VALUE(None), ENUM_VALUE(Flag1), ENUM_VALUE(Flag2),
	ENUM_VALUE(Flag3), ENUM_VALUE(Flag4), ENUM_VALUE(Flag5));

class PrimitiveTypesClass : public Reflection::Object
{
	DECLARE_REFLECTION_CLASS();
public:
	int integer = 0;
	float singlePrecision = 0.0f;
	double doublePrecision = 0.0;
	ENormalEnum enumNormal = ENormalEnum::SomeValue;
	EFlags enumFlags = EFlags::None;
};
IMPLEMENT_REFLECTION_CLASS(PrimitiveTypesClass, Reflection::Object)
{
	ADD_FIELD(integer);
	ADD_FIELD(singlePrecision);
	ADD_FIELD(doublePrecision);
	ADD_FIELD(enumNormal);
	ADD_FIELD(enumFlags);
}

class StringTypesClass : public Reflection::Object
{
	DECLARE_REFLECTION_CLASS();
public:
	std::string s1, s2, s3;
};
IMPLEMENT_REFLECTION_CLASS(StringTypesClass, Reflection::Object)
{
	ADD_FIELD(s1);
	ADD_FIELD(s2);
	ADD_FIELD(s3);
}

class InheritedClass : public PrimitiveTypesClass
{
	DECLARE_REFLECTION_CLASS();
public:
	unsigned int unsignedInt = 0;
};
IMPLEMENT_REFLECTION_CLASS(InheritedClass, PrimitiveTypesClass)
{
	ADD_FIELD(unsignedInt);
}

class MiddleHierarchyLevelClass : public Reflection::Object
{
	DECLARE_REFLECTION_CLASS();
public:
	std::string ownData = "middle hierarchy level";
	PrimitiveTypesClass primitiveData;
	InheritedClass* inheritedData = new InheritedClass();
};
IMPLEMENT_REFLECTION_CLASS(MiddleHierarchyLevelClass, Reflection::Object)
{
	ADD_FIELD(ownData);
	ADD_FIELD(primitiveData);
	ADD_FIELD(inheritedData);
}

class TopHierarchyLevelClass : public MiddleHierarchyLevelClass
{
	DECLARE_REFLECTION_CLASS();
public:
	MiddleHierarchyLevelClass* secondData = new MiddleHierarchyLevelClass();
};
IMPLEMENT_REFLECTION_CLASS(TopHierarchyLevelClass, MiddleHierarchyLevelClass)
{
	ADD_FIELD(secondData);
}

class ObjectWithVector : public Reflection::Object
{
	DECLARE_REFLECTION_CLASS();
public:
	std::vector<PrimitiveTypesClass> vec;
};
IMPLEMENT_REFLECTION_CLASS(ObjectWithVector, Reflection::Object)
{
	ADD_FIELD(vec);
}

class ObjectWithVectorOfPtrs : public Reflection::Object
{
	DECLARE_REFLECTION_CLASS();
public:
	std::vector<PrimitiveTypesClass*> vec;
};
IMPLEMENT_REFLECTION_CLASS(ObjectWithVectorOfPtrs, Reflection::Object)
{
	ADD_FIELD(vec);
}

#pragma endregion

namespace UnitTests
{
	TEST_CLASS(BinarySerializationTests)
	{
		TEST_METHOD(PrimitiveTypes)
		{
			PrimitiveTypesClass obj1;
			obj1.integer = 1234;
			obj1.singlePrecision = 12.48f;
			obj1.doublePrecision = 0.123456789;
			obj1.enumNormal = YetAnotherValue;
			obj1.enumFlags = EFlags(Flag1 | Flag4 | Flag5);

			auto bytes = BinarySerializer::WriteObject(&obj1);
			PrimitiveTypesClass* obj2 = BinarySerializer::ReadObjectAs<PrimitiveTypesClass>(bytes);
			Assert::IsNotNull(obj2);

			Assert::AreEqual(obj1.integer, obj2->integer);
			Assert::AreEqual(obj1.singlePrecision, obj2->singlePrecision);
			Assert::AreEqual(obj1.doublePrecision, obj2->doublePrecision);
			Assert::AreEqual((int)obj1.enumNormal, (int)obj2->enumNormal);
			Assert::AreEqual((int)obj1.enumFlags, (int)obj2->enumFlags);
			delete obj2;
		}

		TEST_METHOD(StringTypes)
		{
			StringTypesClass obj1;
			obj1.s1 = "abcd";
			obj1.s2 = "hello world";
			obj1.s3 = "Ala ma kota.\nA kot nie ma \\Ali.";

			auto bytes = BinarySerializer::WriteObject(&obj1);
			StringTypesClass* obj2 = BinarySerializer::ReadObjectAs<StringTypesClass>(bytes);
			Assert::IsNotNull(obj2);

			obj1.s1 = "defg";

			Assert::AreEqual(std::string("abcd"), obj2->s1);
			Assert::AreEqual(obj1.s2, obj2->s2);
			Assert::AreEqual(obj1.s3, obj2->s3);

			delete obj2;
		}

		TEST_METHOD(HierarchySimple)
		{

		}

		TEST_METHOD(HierarchyNull)
		{

		}
	};

	TEST_CLASS(JsonSerialization)
	{
		TEST_METHOD(PrimitiveTypes)
		{
			PrimitiveTypesClass obj1;
			obj1.integer = 1234;
			obj1.singlePrecision = 12.48f;
			obj1.doublePrecision = 0.123456789;
			obj1.enumNormal = YetAnotherValue;
			obj1.enumFlags = EFlags(Flag1 | Flag4 | Flag5);

			std::string json = Reflection::ToString(obj1);
			Assert::AreEqual(json, std::string(R"({
  "fields": {
    "doublePrecision": 0.123456789,
    "enumFlags": 25,
    "enumNormal": 2,
    "integer": 1234,
    "singlePrecision": 12.479999542236328
  }
})"));

			PrimitiveTypesClass obj2;
			Assert::IsTrue(Reflection::FromString(json, obj2));
				
			Assert::AreEqual(obj1.integer, obj2.integer);
			Assert::AreEqual(obj1.singlePrecision, obj2.singlePrecision);
			Assert::AreEqual(obj1.doublePrecision, obj2.doublePrecision);
			Assert::AreEqual((int)obj1.enumNormal, (int)obj2.enumNormal);
			Assert::AreEqual((int)obj1.enumFlags, (int)obj2.enumFlags);
		}

		TEST_METHOD(ObjectPointer)
		{
			PrimitiveTypesClass* obj1 = new PrimitiveTypesClass();
			obj1->integer = 1234;
			obj1->singlePrecision = 12.48f;
			obj1->doublePrecision = 0.123456789;
			obj1->enumNormal = YetAnotherValue;
			obj1->enumFlags = EFlags(Flag1 | Flag4 | Flag5);

			std::string json = Reflection::ToString(obj1);
			Assert::AreEqual(json, std::string(R"({
  "class": "PrimitiveTypesClass",
  "fields": {
    "doublePrecision": 0.123456789,
    "enumFlags": 25,
    "enumNormal": 2,
    "integer": 1234,
    "singlePrecision": 12.479999542236328
  }
})"));

			PrimitiveTypesClass* obj2 = nullptr;
			Assert::IsTrue(Reflection::FromString(json, obj2));
			Assert::IsNotNull(obj2);

			Assert::AreEqual(obj1->integer, obj2->integer);
			Assert::AreEqual(obj1->singlePrecision, obj2->singlePrecision);
			Assert::AreEqual(obj1->doublePrecision, obj2->doublePrecision);
			Assert::AreEqual((int)obj1->enumNormal, (int)obj2->enumNormal);
			Assert::AreEqual((int)obj1->enumFlags, (int)obj2->enumFlags);
		}

		TEST_METHOD(HierarchySimple)
		{
			MiddleHierarchyLevelClass obj1;
			obj1.ownData = "hello, how are you?";
			obj1.inheritedData->integer = 12;

			std::string json = Reflection::ToString(&obj1);
			Assert::AreEqual(json, std::string(R"({
  "class": "MiddleHierarchyLevelClass",
  "fields": {
    "inheritedData": {
      "class": "InheritedClass",
      "fields": {
        "doublePrecision": 0.0,
        "enumFlags": 0,
        "enumNormal": 0,
        "integer": 12,
        "singlePrecision": 0.0,
        "unsignedInt": 0
      }
    },
    "ownData": "hello, how are you?",
    "primitiveData": {
      "fields": {
        "doublePrecision": 0.0,
        "enumFlags": 0,
        "enumNormal": 0,
        "integer": 0,
        "singlePrecision": 0.0
      }
    }
  }
})"));

			MiddleHierarchyLevelClass* obj2 = nullptr;
			Assert::IsTrue(Reflection::FromString<MiddleHierarchyLevelClass*>(json, obj2));
			Assert::IsNotNull(obj2);

			Assert::AreEqual(obj2->ownData, obj1.ownData);
			Assert::AreEqual(obj2->inheritedData->integer, obj1.inheritedData->integer);
			Assert::AreEqual(obj2->inheritedData->singlePrecision, obj1.inheritedData->singlePrecision);
		}

		TEST_METHOD(HierarchyNull)
		{
			MiddleHierarchyLevelClass obj1;
			delete obj1.inheritedData;
			obj1.inheritedData = nullptr;

			std::string json = Reflection::ToString(obj1);
			Assert::AreEqual(json, std::string(R"({
  "fields": {
    "inheritedData": null,
    "ownData": "middle hierarchy level",
    "primitiveData": {
      "fields": {
        "doublePrecision": 0.0,
        "enumFlags": 0,
        "enumNormal": 0,
        "integer": 0,
        "singlePrecision": 0.0
      }
    }
  }
})"));

			MiddleHierarchyLevelClass obj2;
			Assert::IsTrue(Reflection::FromString(json, obj2));

			Assert::IsNull(obj2.inheritedData);
		}

		TEST_METHOD(HierarchyComplex)
		{
			TopHierarchyLevelClass* obj1 = new TopHierarchyLevelClass();
			obj1->inheritedData = nullptr;
			obj1->secondData->primitiveData.integer = 2;
			obj1->secondData->inheritedData->unsignedInt = 17;

			std::string json = Reflection::ToString(obj1);
			std::string expected = std::string(R"({
  "class": "TopHierarchyLevelClass",
  "fields": {
    "inheritedData": null,
    "ownData": "middle hierarchy level",
    "primitiveData": {
      "fields": {
        "doublePrecision": 0.0,
        "enumFlags": 0,
        "enumNormal": 0,
        "integer": 0,
        "singlePrecision": 0.0
      }
    },
    "secondData": {
      "class": "MiddleHierarchyLevelClass",
      "fields": {
        "inheritedData": {
          "class": "InheritedClass",
          "fields": {
            "doublePrecision": 0.0,
            "enumFlags": 0,
            "enumNormal": 0,
            "integer": 0,
            "singlePrecision": 0.0,
            "unsignedInt": 17
          }
        },
        "ownData": "middle hierarchy level",
        "primitiveData": {
          "fields": {
            "doublePrecision": 0.0,
            "enumFlags": 0,
            "enumNormal": 0,
            "integer": 2,
            "singlePrecision": 0.0
          }
        }
      }
    }
  }
})");
			Assert::AreEqual(expected, json);

			TopHierarchyLevelClass* obj2 = nullptr;
			Assert::IsTrue(Reflection::FromString(json, obj2));
			Assert::IsNotNull(obj2);

			json = Reflection::ToString(obj2);
			Assert::AreEqual(expected, json);
		}

		TEST_METHOD(CollectionOfObjects)
		{
			std::vector<PrimitiveTypesClass> v1(3);
			v1[0].integer = 1;
			v1[1].singlePrecision = 2.0f;
			v1[2].enumFlags = Flag3;

			std::string serialized = Reflection::ToString(v1);
			Assert::AreEqual(serialized, std::string(R"([
  {
    "fields": {
      "doublePrecision": 0.0,
      "enumFlags": 0,
      "enumNormal": 0,
      "integer": 1,
      "singlePrecision": 0.0
    }
  },
  {
    "fields": {
      "doublePrecision": 0.0,
      "enumFlags": 0,
      "enumNormal": 0,
      "integer": 0,
      "singlePrecision": 2.0
    }
  },
  {
    "fields": {
      "doublePrecision": 0.0,
      "enumFlags": 4,
      "enumNormal": 0,
      "integer": 0,
      "singlePrecision": 0.0
    }
  }
])"));

			std::vector<PrimitiveTypesClass> v2;
			Assert::IsTrue(Reflection::FromString(serialized, v2));
			Assert::AreEqual(v1.size(), v2.size());
			Assert::AreEqual(v1[0].integer, v2[0].integer);
			Assert::AreEqual(v1[1].singlePrecision, v2[1].singlePrecision);
			Assert::AreEqual((int)v1[2].enumFlags, (int)v2[2].enumFlags);
		}

		TEST_METHOD(CollectionOfObjectsAsField)
		{
			ObjectWithVector obj1;
			obj1.vec.push_back(PrimitiveTypesClass());
			obj1.vec[0].integer = 1;
			obj1.vec.push_back(PrimitiveTypesClass());
			obj1.vec[1].singlePrecision = 2.0f;
			obj1.vec.push_back(PrimitiveTypesClass());
			obj1.vec[2].doublePrecision = 2.0;

			std::string json = Reflection::ToString(obj1);
			Assert::AreEqual(json, std::string(R"({
  "fields": {
    "vec": [
      {
        "fields": {
          "doublePrecision": 0.0,
          "enumFlags": 0,
          "enumNormal": 0,
          "integer": 1,
          "singlePrecision": 0.0
        }
      },
      {
        "fields": {
          "doublePrecision": 0.0,
          "enumFlags": 0,
          "enumNormal": 0,
          "integer": 0,
          "singlePrecision": 2.0
        }
      },
      {
        "fields": {
          "doublePrecision": 2.0,
          "enumFlags": 0,
          "enumNormal": 0,
          "integer": 0,
          "singlePrecision": 0.0
        }
      }
    ]
  }
})"));
			ObjectWithVector obj2;
			Assert::IsTrue(Reflection::FromString(json, obj2));
			Assert::AreEqual(obj1.vec.size(), obj2.vec.size());
			Assert::AreEqual(obj1.vec[0].integer, obj2.vec[0].integer);
			Assert::AreEqual(obj1.vec[1].singlePrecision, obj2.vec[1].singlePrecision);
			Assert::AreEqual(obj1.vec[2].doublePrecision, obj2.vec[2].doublePrecision);
		}

		TEST_METHOD(CollectionOfPointers)
		{
			std::map<std::string, PrimitiveTypesClass*> m1;
			m1["one"] = nullptr;
			m1["two"] = new PrimitiveTypesClass();
			m1["two"]->integer = 2;
			auto obj3 = new InheritedClass();
			obj3->unsignedInt = 11;
			obj3->singlePrecision = 3.0f;
			m1["three"] = obj3;

			std::string serialized = Reflection::ToString(m1);
			Assert::AreEqual(serialized, std::string(R"({
  "one": null,
  "three": {
    "class": "InheritedClass",
    "fields": {
      "doublePrecision": 0.0,
      "enumFlags": 0,
      "enumNormal": 0,
      "integer": 0,
      "singlePrecision": 3.0,
      "unsignedInt": 11
    }
  },
  "two": {
    "class": "PrimitiveTypesClass",
    "fields": {
      "doublePrecision": 0.0,
      "enumFlags": 0,
      "enumNormal": 0,
      "integer": 2,
      "singlePrecision": 0.0
    }
  }
})"));

			std::map<std::string, PrimitiveTypesClass*> m2;
			Assert::IsTrue(Reflection::FromString(serialized, m2));
			Assert::AreEqual(m1.size(), m2.size());
			Assert::IsNull(m2["one"]);
			Assert::AreEqual(2, m2["two"]->integer);
			Assert::IsTrue(dynamic_cast<InheritedClass*>(m2["three"]));
			Assert::AreEqual(obj3->unsignedInt, reflection_cast<InheritedClass>(m2["three"])->unsignedInt);
		}

		TEST_METHOD(CollectionOfPointersAsField)
		{
			ObjectWithVectorOfPtrs* obj1 = new ObjectWithVectorOfPtrs();
			obj1->vec.push_back(new PrimitiveTypesClass());
			obj1->vec[0]->enumFlags = EFlags(Flag5 | Flag3);
			obj1->vec.push_back(nullptr);
			obj1->vec.push_back(new InheritedClass());
			static_cast<InheritedClass*>(obj1->vec[2])->unsignedInt = 3;
			obj1->vec[2]->integer = 3;

			std::string json = Reflection::ToString(obj1);
			Assert::AreEqual(json, std::string(R"({
  "class": "ObjectWithVectorOfPtrs",
  "fields": {
    "vec": [
      {
        "class": "PrimitiveTypesClass",
        "fields": {
          "doublePrecision": 0.0,
          "enumFlags": 20,
          "enumNormal": 0,
          "integer": 0,
          "singlePrecision": 0.0
        }
      },
      null,
      {
        "class": "InheritedClass",
        "fields": {
          "doublePrecision": 0.0,
          "enumFlags": 0,
          "enumNormal": 0,
          "integer": 3,
          "singlePrecision": 0.0,
          "unsignedInt": 3
        }
      }
    ]
  }
})"));

			ObjectWithVectorOfPtrs* obj2 = nullptr;
			Assert::IsTrue(Reflection::FromString(json, obj2));
			Assert::IsNotNull(obj2);

			Assert::AreEqual(obj1->vec.size(), obj2->vec.size());
			Assert::AreEqual((int)obj1->vec[0]->enumFlags,(int)obj2->vec[0]->enumFlags);
			Assert::IsNull(obj2->vec[1]);
			Assert::IsTrue(dynamic_cast<InheritedClass*>(obj2->vec[2]));
			Assert::AreEqual(static_cast<InheritedClass*>(obj1->vec[2])->unsignedInt,
							static_cast<InheritedClass*>(obj2->vec[2])->unsignedInt);
			Assert::AreEqual(obj1->vec[2]->integer, obj2->vec[2]->integer);
		}

		//#TODO Edge cases and trying to deserialize invalid data
	};
}