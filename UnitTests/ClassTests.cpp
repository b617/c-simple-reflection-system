#include "stdafx.h"
#include <assert.h>
#include "CppUnitTest.h"
#include "Reflection/ReflectionApi.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

class TestClassBase : public Reflection::Object
{
	DECLARE_REFLECTION_CLASS();
public:
	virtual std::string Name() const { return "ClassBase"; }
};
IMPLEMENT_REFLECTION_CLASS(TestClassBase, Reflection::Object) {}

class TestAbstractClass : public TestClassBase
{
	DECLARE_REFLECTION_CLASS();
public:
	TestAbstractClass(const char*) {};
	virtual std::string Name() const override { return "AbstractClass"; }
};
IMPLEMENT_REFLECTION_ABSTRACT_CLASS(TestAbstractClass, TestClassBase) {}


class TestDerivedClass : public TestAbstractClass
{
	DECLARE_REFLECTION_CLASS();
public:
	TestDerivedClass() : TestAbstractClass(nullptr) { data = 78; }
	virtual std::string Name() const { return "DerivedClass"; }

	int data;
};
IMPLEMENT_REFLECTION_CLASS(TestDerivedClass, TestAbstractClass) 
{ 
	ADD_FIELD(data); 
}

class TestUnrelatedClass : public Reflection::Object
{
	DECLARE_REFLECTION_CLASS();
public:

};
IMPLEMENT_REFLECTION_CLASS(TestUnrelatedClass, Reflection::Object) {}


namespace UnitTests
{
	TEST_CLASS(ClassTests)
	{
		TEST_METHOD(SameClassPointer)
		{
			TestClassBase obj;

			const Reflection::Class* c1 = obj.GetClass();
			const Reflection::Class* c2 = TestClassBase::STATIC_CLASS;
			const Reflection::Class* c3 = Reflection::Class::FindClassByName("TestClassBase");

			Assert::AreEqual((int)c1, (int)c2);
			Assert::AreEqual((int)c1, (int)c3);
		}

		TEST_METHOD(ReflectionCast)
		{
			// correct cast
			const TestClassBase* obj = new TestDerivedClass();
			Assert::IsTrue(reflection_cast<TestDerivedClass>(obj) == obj);

			// invalid cast: object of base class
			delete obj;
			obj = new TestClassBase();
			Assert::IsFalse(reflection_cast<TestDerivedClass>(obj) == obj);

			// invalid cast: objects are not connected via inheritance
			//#NOTE commented out since it's been replaced with compile-time error
			//Assert::IsNull(reflection_cast<TestUnrelatedClass>(obj));

			// invalid cast: class with invalid base
			//#NOTE this case is handled by compile-time error

			delete obj;
		}

		TEST_METHOD(CreateInstance)
		{
			const Reflection::Class* c = TestDerivedClass::STATIC_CLASS;
			Reflection::Object* instance = c->CreateObject();

			// check if instance of correct class was created
			TestDerivedClass* castedInstance = reflection_cast<TestDerivedClass>(instance);
			Assert::IsNotNull(castedInstance);
			Assert::IsTrue(castedInstance->Name() == "DerivedClass");

			// check if c-tor got called
			Assert::IsTrue(castedInstance->data == 78);

			delete instance;
		}

		TEST_METHOD(Abstract)
		{
			// abstract class
			Assert::IsTrue(TestAbstractClass::STATIC_CLASS->IsAbstract());
			
			// regular class
			Assert::IsFalse(TestClassBase::STATIC_CLASS->IsAbstract());
			
			// regular class derived from abstract class
			Assert::IsFalse(TestDerivedClass::STATIC_CLASS->IsAbstract());
		}

		TEST_METHOD(Inheritance)
		{
			// GetParentClass()
			Assert::IsTrue(TestAbstractClass::STATIC_CLASS->GetParentClass() == TestClassBase::STATIC_CLASS);
			Assert::IsFalse(TestDerivedClass::STATIC_CLASS->GetParentClass() == TestClassBase::STATIC_CLASS);

			// IsChildOf()
			Assert::IsTrue(TestDerivedClass::STATIC_CLASS->IsChildOf(TestAbstractClass::STATIC_CLASS));
			Assert::IsTrue(TestDerivedClass::STATIC_CLASS->IsChildOf(TestClassBase::STATIC_CLASS));

			Assert::IsFalse(TestDerivedClass::STATIC_CLASS->IsChildOf(TestUnrelatedClass::STATIC_CLASS));
			Assert::IsFalse(TestClassBase::STATIC_CLASS->IsChildOf(TestDerivedClass::STATIC_CLASS));
		}
	};
}