#include "stdafx.h"
#include <assert.h>
#include "CppUnitTest.h"
#include "Reflection/ReflectionApi.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

class Reflection_TestClass_Setters_Getters : public Reflection::Object
{
	DECLARE_REFLECTION_CLASS();

public:
	double double_field;
	unsigned int uint_field;
	float float_field;

	std::string last_function_called_name;

	void DoubleFieldSetter(const double& d) { last_function_called_name = "DoubleFieldSetter"; double_field = d; }
	unsigned int& UintFieldGetter() { last_function_called_name = "UintFieldGetter"; return uint_field; }
	virtual void FloatFieldSetter(const float& f) { last_function_called_name = "FloatFieldSetter"; float_field = f; }

};
IMPLEMENT_REFLECTION_CLASS(Reflection_TestClass_Setters_Getters, Reflection::Object)
{
	ADD_FIELD(double_field)->SetSetter(&Reflection_TestClass_Setters_Getters::DoubleFieldSetter);
	ADD_FIELD(uint_field)->SetGetter(&Reflection_TestClass_Setters_Getters::UintFieldGetter);
	ADD_FIELD(float_field)->SetSetter(&Reflection_TestClass_Setters_Getters::FloatFieldSetter);
}

class Reflection_TestClass_Childclass : public Reflection_TestClass_Setters_Getters
{
	DECLARE_REFLECTION_CLASS();
public:
	virtual void FloatFieldSetter(const float& f) override { last_function_called_name = "FloatFieldSetter (override)"; float_field = f - 1; }
};
IMPLEMENT_REFLECTION_CLASS(Reflection_TestClass_Childclass, Reflection_TestClass_Setters_Getters) {}


namespace UnitTests
{
	TEST_CLASS(FieldTestsGettersSetters)
	{
	public:

		TEST_METHOD(CustomSettersGetters)
		{
			Reflection_TestClass_Setters_Getters instance;
			const Reflection::Field* uint_field = instance.GetClass()->FindField("uint_field");
			const Reflection::Field* double_field = instance.GetClass()->FindField("double_field");

			std::string tmp = uint_field->GetValueAsString(&instance);
			Assert::AreEqual(instance.last_function_called_name.c_str(), "UintFieldGetter");
			double_field->SetValueFromString(&instance, "23.48");
			Assert::AreEqual(instance.last_function_called_name.c_str(), "DoubleFieldSetter");
		}

		TEST_METHOD(CustomSettersGettersOverrides)
		{
			Reflection_TestClass_Setters_Getters parent_instance;
			Reflection_TestClass_Childclass child_instance;

			auto* parent_field = parent_instance.GetClass()->FindField("float_field");
			auto* child_field = child_instance.GetClass()->FindField("float_field");
			

			parent_field->SetValueFromString(&parent_instance, "2");
			Assert::AreEqual(parent_instance.last_function_called_name.c_str(), "FloatFieldSetter");

			parent_field->SetValueFromString(&child_instance, "3");
			Assert::AreEqual(child_instance.last_function_called_name.c_str(), "FloatFieldSetter (override)");


			parent_instance.last_function_called_name = "";
			child_instance.last_function_called_name = "";

			child_field->SetValueFromString(&parent_instance, "4");
			Assert::AreEqual(parent_instance.last_function_called_name.c_str(), "FloatFieldSetter");

			child_field->SetValueFromString(&child_instance, "5");
			Assert::AreEqual(child_instance.last_function_called_name.c_str(), "FloatFieldSetter (override)");
		}
	};

}


