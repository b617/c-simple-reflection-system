#include "stdafx.h"
#include "BinarySerializer.h"
#include "../Reflection/Field.h"

Reflection::Object* BinarySerializer::ReadObject(const Bytes& bytes)
{
	using namespace Reflection;
	size_t currIdx = 0;

	// Read class name
	Bytes::const_iterator classNameEndIter = std::find(bytes.begin(), bytes.end(), '\0');
	if (classNameEndIter == bytes.end())
		return nullptr;

	currIdx = classNameEndIter - bytes.begin() + 1;

	std::string className(bytes.begin(), classNameEndIter);
	Class* clazz = Class::FindClassByName(className);
	if (!clazz || clazz->IsAbstract())
		return nullptr;

	// Create object and read into it
	Object* object = clazz->CreateObject();
	for (const Field* field : object->GetClass()->GetFields())
	{
		unsigned char* ptrBegin = static_cast<unsigned char*>(field->GetVoidPtr(const_cast<Object*>(object)));
		memcpy(ptrBegin, &*bytes.cbegin() + currIdx, field->SizeOf());
		currIdx += field->SizeOf();
	}
	return object;
}

BinarySerializer::Bytes BinarySerializer::WriteObject(const Reflection::Object* object)
{
	using namespace Reflection;
	Bytes bytes;

	// Write class name
	const std::string& className = object->GetClass()->GetName();
	bytes.insert(bytes.begin(), className.begin(), className.end());
	bytes.push_back('\0');

	// Write fields
	for (const Field* field : object->GetClass()->GetFields())
	{
		unsigned char* ptrBegin = static_cast<unsigned char*>(field->GetVoidPtr(const_cast<Object*>(object)));
		unsigned char* ptrEnd = ptrBegin + field->SizeOf();
		bytes.insert(bytes.end(), ptrBegin, ptrEnd);
	}

	return bytes;
}
