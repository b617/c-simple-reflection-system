#pragma once
#include <vector>
#include "../Reflection/ReflectionApi.h"

namespace BinarySerializer
{
	using Bytes = std::vector<unsigned char>;

	Reflection::Object* ReadObject(const Bytes& bytes);

	template<class T>
	T* ReadObjectAs(const Bytes& bytes)
	{
		Reflection::Object* object = ReadObject(bytes);
		if (object)
		{
			T* casted = reflection_cast<T>(object);
			if (casted)
				return casted;

			delete object;
		}
		return nullptr;
	}

	Bytes WriteObject(const Reflection::Object* object);
};

