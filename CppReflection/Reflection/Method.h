#pragma once
#include <string>
#include "Object.h"
#include "StringConversions.h"
#include <vector>

namespace Reflection
{
	template<typename TReturnType, typename... TArgs>
	class MethodT;

	class Method
	{
	public:
		enum MethodCallResult
		{
			Success,
			TooManyArguments,
			TooFewArguments,
			UnableToParse
		};

	protected:
		Method(const char* name)
			:name(name)
		{}
	public:
		virtual ~Method() {};

		std::string GetName() const { return name; };

		virtual size_t GetNumParameters() const = 0;

		//#TODO some kind of check against bad cast to replace dynamic_cast
		template<typename TReturnType, typename... TArgs>
		const MethodT<TReturnType, TArgs...>* Cast() const { return dynamic_cast<const MethodT<TReturnType, TArgs...>*>(this); }

		/// Calls method on target objects, parses method parameters from string args. Returns pointer to actual return value
		virtual MethodCallResult CallFromString(Reflection::Object* target, void* returnValue, const std::vector<std::string>& args) = 0;

	protected:
		std::string name;
	};

	template<typename TReturnType, typename... TArgs>
	class MethodT : public Method
	{
	protected:
		MethodT(const char* name) : Method(name) {}
	public:
		virtual TReturnType Invoke(class Reflection::Object* target, TArgs... args) const = 0;
		virtual size_t GetNumParameters() const override { return sizeof...(TArgs); }
	};

	template<typename TClassType, typename TReturnType, typename... TArgs>
	class MethodTImpl : public MethodT<TReturnType, TArgs...>
	{
	public:
		typedef TReturnType(TClassType::* TFunctionPointer)(TArgs...);
		typedef TReturnType(TClassType::* TConstFunctionPointer)(TArgs...) const;

		MethodTImpl(const char* name, TFunctionPointer address) : MethodT<TReturnType, TArgs...>(name), address(address)
		{}
		MethodTImpl(const char* name, TConstFunctionPointer address) : MethodT<TReturnType, TArgs...>(name), constAddress(address)
		{}

	public:
		virtual ~MethodTImpl() { address = nullptr; }

		TFunctionPointer GetFunctionPointer() const { return (TFunctionPointer)address; }
		TConstFunctionPointer GetConstFunctionPointer() const { return (TConstFunctionPointer)constAddress; }

		TReturnType Invoke(TClassType* target, TArgs... args) const
		{
			TFunctionPointer ptr = GetFunctionPointer();
			return (target->*ptr)(args...);
		}

		virtual TReturnType Invoke(class Reflection::Object* target, TArgs... args) const override { return Invoke((TClassType*)target, args...); }
		virtual Method::MethodCallResult CallFromString(Reflection::Object* target, void* returnValue, const std::vector<std::string>& args) override
		{
			return TryParseAndInvoke(static_cast<TClassType*>(target), (TReturnType*)returnValue, args);
		}

	protected:

		template<typename T = TReturnType>
		inline typename std::enable_if<!std::is_void<T>::value, void>::type InvokeParsedMethod(TClassType* target, TReturnType* returnValue, TArgs... args)
		{
			if (returnValue)
				*returnValue = Invoke(target, args...);
			else
				Invoke(target, args...);
		}

		template<typename T = TReturnType>
		inline typename std::enable_if<std::is_void<T>::value, void>::type InvokeParsedMethod(TClassType* target, TReturnType* returnValue, TArgs... args)
		{
			// Special case for void methods (no return value given)
			Invoke(target, args...);
		}
		
		Method::MethodCallResult TryParseAndInvoke(TClassType* target, TReturnType* returnValue, const std::vector<std::string>& stringArgs, TArgs... args)
		{
			constexpr int existingArgs = sizeof...(args);

			if (stringArgs.size() == existingArgs)
			{
				InvokeParsedMethod(target, returnValue, args...);
				return Method::MethodCallResult::Success;
			}
			else
			{
				return Method::MethodCallResult::TooManyArguments;
			}
		}

		template<typename... TIntermediateArgs>
		Method::MethodCallResult TryParseAndInvoke(TClassType* target, TReturnType* returnValue, const std::vector<std::string>& stringArgs, TIntermediateArgs... args)
		{
			constexpr int existingArgs = sizeof...(args);
			constexpr int requiredArgs = sizeof...(TArgs);
			static_assert(existingArgs < requiredArgs, "Error while compiling Method::TryParseAndInvoke. Invalid arguments were parsed.");

			// Parse next argument and call recursively
			constexpr int nextArgIndex = requiredArgs - existingArgs - 1;
			using TNextArgType = typename std::tuple_element<nextArgIndex, std::tuple<TArgs...>>::type;
			using TNextArgParsableType = typename std::remove_const<typename std::remove_reference<TNextArgType>::type>::type;

			if (stringArgs.size() <= nextArgIndex)
			{
				return Method::MethodCallResult::TooFewArguments;
			}

			TNextArgParsableType value;
			if (!Reflection::FromString(stringArgs[nextArgIndex], value))
			{
				return Method::MethodCallResult::UnableToParse;
			}
			TNextArgType parsedValue = value;

			return TryParseAndInvoke(target, returnValue, stringArgs, args..., parsedValue);
		}

	protected:
		union
		{
			TFunctionPointer address;
			TConstFunctionPointer constAddress;
		};
	};
}

#define MAKE_METHOD(_class, _return_type, _function, ...) new Reflection::MethodTImpl<_class, _return_type, __VA_ARGS__>(#_function, &_class :: _function)
