#include "stdafx.h"
#include "Class.h"
#include "Field.h"
#include "Method.h"
#include <algorithm>

std::map<std::string, Reflection::Class*> Reflection::Class::classes;

Reflection::Class::Class(const char* class_name, const char* parent_class_name)
	: name(class_name)
{
	// Register class
	classes[std::string(name)] = this;
	
	// Find parent class
	parent_class = nullptr;
	auto find_result = classes.find(parent_class_name);
	if (find_result != classes.end())
	{
		parent_class = find_result->second;
	}

}

const std::vector<const Reflection::Field*> Reflection::Class::GetFields() const
{
	std::vector<const Reflection::Field*> returnValue;

	const Class* currClass = this;
	while (currClass)
	{
		std::copy(currClass->fields.begin(), currClass->fields.end(), std::back_inserter(returnValue));
		currClass = currClass->GetParentClass();
	}
	return returnValue;
}

Reflection::Field* Reflection::Class::FindField(const std::string& name) const
{
	for (auto it = fields.begin(); it != fields.end(); ++it)
	{
		if ((*it)->GetName() == name)
			return *it;
	}

	if (parent_class)
		return parent_class->FindField(name);
	else
		return nullptr;
}

Reflection::Method* Reflection::Class::FindMethod(const std::string& name) const
{
	for (auto it = methods.begin(); it != methods.end(); ++it)
	{
		if ((*it)->GetName() == name)
			return *it;
	}

	if (parent_class)
		return parent_class->FindMethod(name);
	else
		return nullptr;
}

bool Reflection::Class::IsChildOf(const Class* parent_class) const
{
	const Class* c = this;
	while (c != nullptr)
	{
		if (c == parent_class)
			return true;

		c = c->GetParentClass();
	}

	return false;
}


void Reflection::Class::__Finalize()
{
	// Sort vectors
	std::sort(fields.begin(), fields.end());
	fields.shrink_to_fit();

	std::sort(methods.begin(), methods.end());
	methods.shrink_to_fit();
}

Reflection::Class* Reflection::Class::FindClassByName(const std::string& name)
{
	auto result = classes.find(name);
	
	if (result == classes.end())
		return nullptr;
	else
		return result->second;
}

void Reflection::ShutdownReflectionSystem()
{
	for (auto c : Class::classes)
	{
		Class* clazz = c.second;
		
		// delete fields
		for (Field* f : clazz->fields)
			delete f;

		clazz->fields.clear();

		// delete methods
		for (Method* m : clazz->methods)
			delete m;
		
		clazz->methods.clear();

		// delete classes
		delete clazz;
	}
	Class::classes.clear();
}
