#pragma once
#include <map>
#include <string>
#include <type_traits>
#include <queue>
#include "StringConversions.h"

namespace Reflection
{
	class Enum
	{
	public:
		std::string GetName() const
		{
			return name;
		}
		bool IsFlags() const
		{
			return is_flags;
		}

		static const Enum* FindEnumByName(const std::string& name);

	protected:
		Enum(const char* name, bool is_flags);

		bool EqualIgnoreNamespaces(const std::string& first, const std::string& second) const;
		std::string RemoveNamespaces(const std::string& name_with_namespaces) const;

		std::string name;
		bool is_flags;

	private:
		static std::map<std::string, Enum*> enums;
	};

	template<typename TEnumClass>
	class EnumImpl : public Enum
	{
		static_assert(std::is_enum<TEnumClass>::value, "REFLECTION_IMPLEMENT_ENUM() can only be used with enums!");

	public:
		EnumImpl(const char* name, bool is_flags, TEnumClass default_value, const char* default_value_as_string)
			: Enum(name, is_flags)
			, default_value(default_value)
			, default_value_as_string(RemoveNamespaces(default_value_as_string))
		{
		}

		bool FromString(const std::string& in_string, TEnumClass& outValue) const
		{
			if (is_flags)
			{
				outValue = (TEnumClass)(0); 
				bool found_something = false;
				
				for (const auto& name_and_value : enum_values)
				{
					if (in_string.find( RemoveNamespaces(name_and_value.first) ) != std::string::npos)
					{
						outValue = (TEnumClass)((int)outValue | (int)name_and_value.second);
						found_something = true;
					}
				}

				if (!found_something)
				{
					outValue = default_value;
				}

				return found_something;
			}
			else
			{
				for (const auto& name_and_value : enum_values)
				{
					if (EqualIgnoreNamespaces(name_and_value.first, in_string))
					{
						outValue = name_and_value.second;
						return true;
					}
				}

				outValue = default_value;
				return false;
			}
		}
		std::string ToString(const TEnumClass& value) const
		{
			if (is_flags)
			{
				std::string result = "";
				for (const auto& name_and_value : enum_values)
				{
					if ((int)value & (int)name_and_value.second)
					{
						result += RemoveNamespaces(name_and_value.first) + '|';
					}
				}

				if (result.size() != 0)
				{
					return result.substr(0, result.size() - 1);
				}
			}
			else
			{
				for (const auto& name_and_value : enum_values)
				{
					if (value == name_and_value.second)
					{
						return RemoveNamespaces(name_and_value.first);
					}
				}
			}

			return default_value_as_string;
		}

	private:
		template<typename TEnumClass>
		friend EnumImpl<TEnumClass>* CreateReflectionEnumImplementation(const char* name, bool is_flags, TEnumClass default_value,
			const char* default_value_as_string, TEnumClass value, const char* value_as_string);

		template<typename TEnumClass, typename... Values>
		friend EnumImpl<TEnumClass>* CreateReflectionEnumImplementation(const char* name, bool is_flags, TEnumClass default_value,
			const char* default_value_as_string, TEnumClass value, const char* value_as_string, Values... values);

		inline void AddEnumValue(const char* name, const TEnumClass value)
		{
			enum_values.push_front(std::pair<std::string, TEnumClass>(name, value));
		}

		std::deque<std::pair<std::string, TEnumClass>> enum_values;
		TEnumClass default_value;
		std::string default_value_as_string;

		static EnumImpl* instance;
	};


	template<typename TEnumClass>
	EnumImpl<TEnumClass>* CreateReflectionEnumImplementation(const char* name, bool is_flags, TEnumClass default_value, const char* default_value_as_string,
		TEnumClass value, const char* value_as_string)
	{
		Reflection::EnumImpl<TEnumClass>* e = new Reflection::EnumImpl<TEnumClass>(name, is_flags, default_value, default_value_as_string);
		e->AddEnumValue(value_as_string, value);
		return e;
	}

	template<typename TEnumClass, typename... Values>
	EnumImpl<TEnumClass>* CreateReflectionEnumImplementation(const char* name, bool is_flags, TEnumClass default_value, const char* default_value_as_string,
		TEnumClass value, const char* value_as_string, Values... values)
	{
		EnumImpl<TEnumClass>* e = CreateReflectionEnumImplementation(name, is_flags, default_value, default_value_as_string, values...);
		e->AddEnumValue(value_as_string, value);
		return e;
	}

}
