#pragma once
#include <string>
#include <nlohmann/json.hpp>

namespace Reflection
{
	class Object;
	using json = nlohmann::json;

	void to_json(json& j, const Object& object);
	void to_json(json& j, Object*const& object);
	void from_json(const json& j, Object& object);
	void from_json(const json& j, Object*& object);

	template<typename T> std::enable_if_t<std::is_base_of<Object,T>::value && !std::is_same<Object,T>::value,
	void> inline from_json(const json& j, T*& object)
	{
		Reflection::Object* obj2 = object;
		from_json(j, obj2);
		object = static_cast<T*>(obj2);
	}

	template<typename T>
	class Converter
	{
	public:
		std::string ToString(const T& value)
		{
			json j = value;
			return j.dump(2);
		}
		template<typename T = T> std::enable_if_t <!std::is_pointer<T>::value ||
		(std::is_pointer<T>::value && !std::is_base_of<Object, typename std::remove_pointer<T>::type >::value),
		bool> FromString(const std::string& inString, T& outValue)
		{
			json j = json::parse(inString, nullptr, false);
			if (!j.is_discarded())
			{
				outValue = j.get<T>();
			}
			return !j.is_discarded();
		}
		template<typename T = T> std::enable_if_t <std::is_pointer<T>::value &&
		std::is_base_of<Object, typename std::remove_pointer<T>::type >::value,
		bool> FromString(const std::string& inString, T& outValue)
		{
			json j = json::parse(inString, nullptr, false);
			if (!j.is_discarded())
			{
				Reflection::Object* obj = outValue;
				from_json(j, obj);
				outValue = static_cast<T>(obj);
			}
			return !j.is_discarded();
		}
	};

	// Converts any reflection-viable parameter to string using json converter
	template<typename T>
	std::string ToString(const T& value)
	{
		Converter<T> converter;
		return converter.ToString(value);
	}

	/**
	* Converts string to any reflection-viable type using json converter
	* @param inString - text from which to read value
	* @param outValue - variable to which will new value be read
	* @returns true if success, false if cannot read from this string
	*/
	template<typename T>
	bool FromString(const std::string& inString, T& outValue)
	{
		Converter<T> converter;
		return converter.FromString(inString, outValue);
	}
}
