#pragma once
#include <string>
#include "StringConversions.h"

namespace Reflection
{
	class Object;
	template<typename T>
	class FieldT;

	class Field
	{
	public:
		Field(const char* name, size_t offset)
			: name(name), offset(offset)
		{}
		~Field() {}


		virtual bool SetValueFromString(Object* target, const std::string& value) const = 0;
		virtual void SetValueFromJson(Object* target, const json& j) const = 0;
		std::string GetName() const { return name; }
		virtual std::string GetValueAsString(Object* target) const = 0;
		virtual json GetValueAsJson(Object* target) const = 0;
		virtual size_t SizeOf() const = 0;
		virtual void* GetVoidPtr(Object* target) const = 0;

		template<typename T>
		const FieldT<T>* AsType() const { return static_cast<const FieldT<T>*>(this); }
		
		enum Flags
		{
			HAS_SETTER = 1,
			SETTER_IS_STATIC = 2,
			HAS_GETTER = 4,
			GETTER_IS_STATIC = 8
		};

	protected:
		size_t offset;
		const char* name;
	};

	template<typename T>
	class FieldT : public Field
	{
	public:
		FieldT(const char* name, size_t offset)
			: Field(name, offset)
		{}

		T* GetPointer(Object* target) const
		{
			return (T*)((char*)target + offset);
		}
		virtual void* GetVoidPtr(Object* target) const override { return GetPointer(target); }

		virtual bool SetValueFromString(Object* target, const std::string& value) const override
		{
			return FromString<T>(value, *GetPointer(target));
		}
		virtual void SetValueFromJson(Object* target, const json& j) const override
		{
			SetValueFromJsonImpl(target, j);
		}
		virtual void SetValue(Object* target, const T& value) const
		{
			*GetPointer(target) = value;
		}
		virtual std::string GetValueAsString(Object* target) const override
		{
			return ToString<T>(*GetPointer(target));
		}
		virtual json GetValueAsJson(Object* target) const override
		{
			return *GetPointer(target);
		}
		virtual const T& GetValue(Object* target) const
		{
			return *GetPointer(target);
		}
		virtual size_t SizeOf() const override
		{
			return sizeof(T);
		}

	private:
		template<typename T = T> std::enable_if_t < !std::is_pointer<T>::value ||
		(std::is_pointer<T>::value && !std::is_base_of<Object, typename std::remove_pointer<T>::type >::value),
		void> SetValueFromJsonImpl(Object* target, const json& j) const
		{
			*GetPointer(target) = j.get<T>();
		}
		template<typename T = T> std::enable_if_t <std::is_pointer<T>::value &&
		std::is_base_of<Object, typename std::remove_pointer<T>::type >::value,
		void> SetValueFromJsonImpl(Object* target, const json& j) const
		{
			Reflection::Object* obj = *GetPointer(target);
			from_json(j, obj);
			*GetPointer(target) = static_cast<T>(obj);
		}
	};

	template<class TClassType, typename TFieldType>
	class FieldTImpl : public FieldT<TFieldType>
	{
	public:
		FieldTImpl(const char* name, size_t offset)
			: FieldT<TFieldType>(name, offset)
		{}

	private:
		typedef FieldTImpl<TClassType, TFieldType> this_t;
	public:
		typedef void(*TStaticSetter)(TClassType*, const TFieldType&);
		typedef void(TClassType::*TMemberSetter)(const TFieldType&);
		typedef TFieldType&(*TStaticGetter)(const TClassType*);
		typedef TFieldType&(TClassType::*TMemberGetter)(void);

	protected:
		union TSetterPointers
		{
			TStaticSetter static_setter = nullptr;
			TMemberSetter member_setter;
		} setter_pointers;
		union TGetterPointers
		{
			TStaticGetter static_getter = nullptr;
			TMemberGetter member_getter;
		} getter_pointers;

		unsigned char flags = (unsigned char)(0);

	public:

		this_t* SetSetter(TStaticSetter function) { flags |= Field::HAS_SETTER | Field::SETTER_IS_STATIC; setter_pointers.static_setter = function; return this; }
		this_t* SetSetter(TMemberSetter function) { flags |= Field::HAS_SETTER; flags &= ~Field::SETTER_IS_STATIC; setter_pointers.member_setter = function; return this; }

		this_t* SetGetter(TStaticGetter function) { flags |= Field::HAS_GETTER | Field::GETTER_IS_STATIC; getter_pointers.static_getter = function; return this; }
		this_t* SetGetter(TMemberGetter function) { flags |= Field::HAS_GETTER; flags &= ~Field::GETTER_IS_STATIC; getter_pointers.member_getter = function; return this; }


		virtual bool SetValueFromString(Object* target, const std::string& value) const override
		{
			TFieldType tmp;
			const bool success = FromString(value, tmp);
			if (success)
			{
				SetValue(target, tmp);
			}
			return success;
		}
		virtual void SetValue(Object* target, const TFieldType& value) const override
		{
			if (flags&Field::HAS_SETTER)
			{
				if (flags&Field::SETTER_IS_STATIC)
					setter_pointers.static_setter(static_cast<TClassType*>(target), value);
				else
					(static_cast<TClassType*>(target)->*setter_pointers.member_setter)(value);
			}
			else
			{
				*this->GetPointer(target) = value;
			}
		}
		virtual std::string GetValueAsString(Object* target) const override
		{
			const TFieldType& tmp = GetValue(target);
			return ToString(tmp);
		}
		virtual const TFieldType& GetValue(Object* target) const override
		{
			if (flags&Field::HAS_GETTER)
			{
				if (flags&Field::GETTER_IS_STATIC)
					return getter_pointers.static_getter(static_cast<TClassType*>(target));
				else
					return (static_cast<TClassType*>(target)->*getter_pointers.member_getter)();
			}
			else
			{
				return *this->GetPointer(target);
			}
		}
	};

}

template<typename T, typename U> constexpr size_t offsetOf(U T::*member)
{
	return (char*)&((T*)nullptr->*member) - (char*)nullptr;
}

#define MAKE_FIELD(_class, _type, _var) new Reflection::FieldTImpl<_class, _type>(#_var, offsetOf(&_class::##_var))
