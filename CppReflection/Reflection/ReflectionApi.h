#pragma once
#include "StringConversions.h"
#include "Class.h"
#include "Field.h"
#include "Object.h"
#include "Method.h"
#include "Enum.h"

template<class TargetClass, class BaseClass>
const TargetClass* reflection_cast(const BaseClass* obj)
{
	static_assert(std::is_base_of<Reflection::Object, BaseClass>::value, "reflection_cast can only be used on object that derive from Reflection::Object!");

	if (obj->GetClass()->IsChildOf(TargetClass::STATIC_CLASS))
		return static_cast<const TargetClass*>(obj);
	else
		return nullptr;
}
template<class TargetClass, class BaseClass>
TargetClass* reflection_cast(BaseClass* obj)
{
	// Run above cast (const version) and remove const modifier to avoid code duplication
	return const_cast<TargetClass*>(reflection_cast<TargetClass, BaseClass>(const_cast<const BaseClass*>(obj)));
}

//////////////////////////////////////////////////////////////////////////
//								CLASS									//
//////////////////////////////////////////////////////////////////////////

#define DECLARE_REFLECTION_CLASS()																									\
public: static const Reflection::Class* STATIC_CLASS;																				\
virtual const Reflection::Class* GetClass() const override { return STATIC_CLASS; }													\
private: template<typename ThisClassName> static void __internal_class_init(Reflection::Class* reflection_class);					\
struct StaticConstructor { StaticConstructor(); }; friend struct StaticConstructor; static StaticConstructor __internal_static_ctor;

/// Internal use only, part of IMPLEMENT_REFLECTION_CLASS and IMPLEMENT_REFLECTION_ABSTRACT_CLASS macros
#define ___REFLECTION_INTERNAL_IMPLEMENT_REFLECTION_CLASS(class_name, parent_class)													\
static_assert(!std::is_same<class_name, parent_class>::value, "Invalid reflection declaration: Class cannot be it's own parent.");	\
static_assert(std::is_base_of<parent_class, class_name>::value, "Invalid reflection declaration: Class "							\
#class_name " does not derive from " #parent_class ".");																			\
class_name##::StaticConstructor class_name##::__internal_static_ctor;																\
class_name##::StaticConstructor::StaticConstructor()																				\
{																																	\
	class_name##::__internal_class_init<class_name>(const_cast<Reflection::Class*>(class_name##::STATIC_CLASS));					\
}																																	\
template<typename ThisClassName>																									\
void class_name##::__internal_class_init(Reflection::Class* reflection_class)														\

#define IMPLEMENT_REFLECTION_CLASS(class_name, parent_class)																		\
const Reflection::Class* class_name##::STATIC_CLASS = new Reflection::ClassT<class_name>(#class_name, #parent_class);				\
___REFLECTION_INTERNAL_IMPLEMENT_REFLECTION_CLASS(class_name, parent_class)

#define IMPLEMENT_REFLECTION_ABSTRACT_CLASS(class_name, parent_class)																\
const Reflection::Class* class_name##::STATIC_CLASS = new Reflection::Class(#class_name, #parent_class);							\
___REFLECTION_INTERNAL_IMPLEMENT_REFLECTION_CLASS(class_name, parent_class)


//////////////////////////////////////////////////////////////////////////
//								FIELD									//
//////////////////////////////////////////////////////////////////////////

#define ADD_FIELD(field) \
static_cast<Reflection::FieldTImpl<ThisClassName,decltype(ThisClassName::field)>*>( \
reflection_class->__AddField( MAKE_FIELD(ThisClassName, decltype(ThisClassName::field), field) ))

#define ADD_METHOD(return_type, method, ...) reflection_class->__AddMethod( MAKE_METHOD(ThisClassName, return_type, method, __VA_ARGS__) )


//////////////////////////////////////////////////////////////////////////
//								ENUM									//
//////////////////////////////////////////////////////////////////////////

#define IMPLEMENT_REFLECTION_ENUM(enum_class, default_value, is_flags, ...)																	\
namespace Reflection																														\
{																																			\
	template<> inline std::string ToString<enum_class>(const enum_class& value)																\
	{																																		\
		return static_cast<const EnumImpl<enum_class>*>(Enum::FindEnumByName(#enum_class))->ToString(value);								\
	}																																		\
	template<> inline bool FromString<enum_class>(const std::string& inString, enum_class& outValue)										\
	{																																		\
		return static_cast<const EnumImpl<enum_class>*>(Enum::FindEnumByName(#enum_class))->FromString(inString, outValue);					\
	}																																		\
}																																			\
Reflection::EnumImpl<enum_class>* Reflection::EnumImpl<enum_class>::instance = Reflection::CreateReflectionEnumImplementation<enum_class>(	\
		#enum_class, is_flags, default_value, #default_value, __VA_ARGS__);



#define ENUM_VALUE(value) value, #value
