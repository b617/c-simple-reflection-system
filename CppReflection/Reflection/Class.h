#pragma once
#include <vector>
#include <map>
#include <string>

namespace Reflection
{
	template<typename TReturnType, typename... TArgs>
	class MethodT;
	class Method;
	class Field;
	class Object;

	class Class
	{
	public:
		Class(const char* class_name, const char* parent_class_name);
		~Class() {}

		virtual Object* CreateObject() const { return nullptr; };

		const std::vector<const Field*> GetFields() const;
		Field* FindField(const std::string& name) const;

		const std::vector<Method*>& GetMethods() const { return methods; };
		Method* FindMethod(const std::string& name) const;
		
		template<typename TReturnType, typename... TArgs>
		MethodT<TReturnType, TArgs...>* FindMethodOverload(const std::string& name) const
		{
			for (auto it = methods.begin(); it != methods.end(); ++it)
			{
				if ((*it)->GetName() == name && dynamic_cast<const MethodT<TReturnType, TArgs...>*>(*it) != nullptr)
					return static_cast<MethodT<TReturnType, TArgs...>*>(*it);
			}

			if (parent_class)
				return parent_class->FindMethodOverload<TReturnType, TArgs...>(name);
			else
				return nullptr;
		}

		Class* GetParentClass() const { return parent_class; };
		bool IsChildOf(const Class* parent_class) const;
		const std::string& GetName() const { return name; }
		virtual bool IsAbstract() const { return true; }

		Field* __AddField(Field* field) { fields.push_back(field); return field; }
		Method* __AddMethod(Method* method) { methods.push_back(method); return method; }
		void __Finalize();
		
	private:

		Class* parent_class;
		std::string name;
		std::vector<Field*> fields;
		std::vector<Method*> methods;

	public:
		static Class* FindClassByName(const std::string& name);

	private:
		static std::map<std::string, Class*> classes;
		friend void ShutdownReflectionSystem();
	};

	template<class T>
	class ClassT : public Class
	{
	public:
		ClassT(const char* class_name, const char* parent_class_name)
			: Class(class_name, parent_class_name)
		{ }

		T* CreateObjectCasted() const { return new T(); }
		virtual bool IsAbstract() const override { return false; }
		virtual Object* CreateObject() const override { return CreateObjectCasted(); }

	};


	void ShutdownReflectionSystem();
}

