#include "stdafx.h"
#include "StringConversions.h"
#include "Object.h"
#include "Class.h"
#include "Field.h"

namespace
{
	const char* CLASS_NAME_TAG = "class";
	const char* FIELDS_ARRAY_TAG = "fields";
}

void Reflection::to_json(json& j, const Object& object)
{
	for (const Field* f : object.GetClass()->GetFields())
	{
		j[FIELDS_ARRAY_TAG][f->GetName()] = f->GetValueAsJson(&const_cast<Object&>(object));
	}
}

void Reflection::to_json(json& j, Object*const& object)
{
	if (object)
	{
		j[CLASS_NAME_TAG] = object->GetClass()->GetName();
		to_json(j, *object);
	}
}

void Reflection::from_json(const json& j, Object& object)
{
	if (j.contains(FIELDS_ARRAY_TAG))
	{
		for (json::const_iterator it = j[FIELDS_ARRAY_TAG].begin(); it != j[FIELDS_ARRAY_TAG].end(); ++it)
		{
			if (const Field* f = object.GetClass()->FindField(it.key()))
			{
				f->SetValueFromJson(&object, it.value());
			}
		}
	}
}

void Reflection::from_json(const json& j, Object*& object)
{
	if (j.is_null())
	{
		object = nullptr;
		return;
	}

	if (!j.contains(CLASS_NAME_TAG) || !j[CLASS_NAME_TAG].is_string())
		return;

	const Class* clazz = Class::FindClassByName(j[CLASS_NAME_TAG]);
	if (!clazz || clazz->IsAbstract())
		return;

	object = clazz->CreateObject();
	from_json(j, *object);
}
