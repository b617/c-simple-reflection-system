#include "stdafx.h"
#include "Enum.h"

namespace Reflection
{

	std::map<std::string, Enum*> Enum::enums;

	const Enum* Enum::FindEnumByName(const std::string& name)
	{
		auto result = enums.find(name);

		if (result == enums.end())
			return nullptr;
		else
			return result->second;
	}

	Enum::Enum(const char* name, bool is_flags)
		: name(name)
		, is_flags(is_flags)
	{
		// Register enum
		enums[std::string(name)] = this;
	}

	bool Enum::EqualIgnoreNamespaces(const std::string& first, const std::string& second) const
	{
		return RemoveNamespaces(first) == RemoveNamespaces(second);
	}

	std::string Enum::RemoveNamespaces(const std::string& name_with_namespaces) const
	{
		std::size_t index = name_with_namespaces.find_last_of(':');

		if (index == std::string::npos)
		{
			return name_with_namespaces;
		}
		else
		{
			return name_with_namespaces.substr(index + 1);
		}
	}

}
