#pragma once

namespace Reflection
{
	class Class;

	/**
	 * Base class for reflectable objects. These objects implement virtual method GetClass()
	 * via DECLARE_REFLECTION_CLASS() macro in header and IMPLEMENT_REFLECTION_CLASS()
	 * or IMPLEMENT_ABSTRACT_REFLECTION_CLASS() macro in source file depending on whether the
	 * class supports default constructor.
	 */
	class Object
	{
	public:
		/**
		 * Returns Reflection::Class* representing the exact class this object is.
		 * You can use it to get access to reflection related functions using this object.
		 */
		virtual const Class* GetClass() const = 0;
	};
}


